package com.ghn.recipiedemo.weather

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.ghn.recipiedemo.R
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class WeatherFragmentTest {

    @Test
    fun testSearchView() {

        val scenario = launchFragmentInContainer<WeatherFragment>()

        val query = "miami"

        onView(withId(R.id.search)).check(matches((isDisplayed())))

        onView(withId(R.id.no_weather_data)).check(matches((isDisplayed())))

        onView(withId(R.id.search_go_btn)).check(matches((isDisplayed())))

        onView(withId(R.id.search_go_btn)).perform(click())

//        onView(withText(R.string.empty_search_error))
//            .inRoot(withDecorView(not(`is`(activityActivityTestRule.activity.window.decorView))))
//            .check(matches(isDisplayed()))

        onView(withId(R.id.search)).perform(clearText(), typeText(query))

        onView(withId(R.id.search_go_btn)).perform(click())
    }
}

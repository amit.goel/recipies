package com.ghn.recipiedemo.nasa.domain

import com.ghn.recipiedemo.di.testNasaModule
import com.ghn.recipiedemo.di.testNetworkModule
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.qualifier.named
import org.koin.test.KoinTest
import org.koin.test.inject
import java.net.HttpURLConnection
import kotlin.test.assertTrue

@ExperimentalCoroutinesApi
class GetNasaImageUseCaseFlowTest : KoinTest {

    private val mockWebServer by inject<MockWebServer>()
    private val testDispatcher by inject<TestCoroutineDispatcher>(
        named("testCoroutineDispatcher")
    )

    private val getNasaImageUseCaseFlow by inject<GetNasaImageUseCaseFlow>(
        named("GetNasaInformationTest")
    )

    private lateinit var mockResponse: MockResponse

    private val errorMessageBody: String = """
		{
            "copyright": "Colleen Pinski",
            "date": "2019-12-25",
            "explanation": "What is this person doing?  In 2012 an annular eclipse of the Sun was visible over a narrow path that crossed the northern Pacific Ocean and several western US states.  In an annular solar eclipse, the Moon is too far from the Earth to block out the entire Sun, leaving the Sun peeking out over the Moon's disk in a ring of fire.  To capture this unusual solar event, an industrious photographer drove from Arizona to New Mexico to find just the right vista. After setting up and just as the eclipsed Sun was setting over a ridge about 0.5 kilometers away, a person unknowingly walked right into the shot. Although grateful for the unexpected human element, the photographer never learned the identity of the silhouetted interloper. It appears likely, though, that the person is holding a circular device that would enable them to get their own view of the eclipse.  The shot was taken at sunset on 2012 May 20 at 7:36 pm local time from a park near Albuquerque, New Mexico, USA. Tomorrow another annular solar eclipse will become visible, this time along a thin path starting in Saudi Arabia and going through southern India, Singapore, and Guam.   However, almost all of Asia with a clear sky will be able to see, tomorrow, at the least, a partial solar eclipse.    Free Download: 2020 APOD Calendar",
            "hdurl": "https://apod.nasa.gov/apod/image/1912/AnnularEclipse_Pinski_1522.jpg",
            "media_type": "image",
            "service_version": "v1",
            "title": "An Annular Solar Eclipse over New Mexico",
            "url": "https://apod.nasa.gov/apod/image/1912/AnnularEclipse_Pinski_960.jpg"
       }
	""".trimIndent()

    private val successMessageBody: String = """
		{"clientId": "9823678"}
	""".trimIndent()

    @Before
    fun setup() {
        startKoin {
            modules(listOf(testNetworkModule, testNasaModule))
        }

        Dispatchers.setMain(testDispatcher)
        mockWebServer.start()

        mockResponse = MockResponse()
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()

        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        stopKoin()
    }

    @Test
    fun `test get login lookup error`() = testDispatcher.runBlockingTest {

        mockResponse.setResponseCode(HttpURLConnection.HTTP_OK)
            .addHeader("content-type: application/json; charset=utf-8")
            .setBody(errorMessageBody)

        mockWebServer.enqueue(mockResponse)

        val destination = mutableListOf<Result<*>>()
        val job = launch {
            getNasaImageUseCaseFlow("2020-01-02").take(2).toList(destination)
        }

//        val result = getNasaImageUseCaseFlow.execute("2020-01-02").take(2).toList()

        assertTrue {
            destination[0] is Result.Loading
        }

        if (destination.size > 1) {
            assertTrue {
                destination[1] is Result.Success
            }
        }

        job.cancel()

//        assertTrue {
//            result is Resource.Success && result.throwable is NCLException
//                    && (result.throwable as NCLException).errorMessage.message == "Error Was detected"
//                    && (result.throwable as NCLException).errorMessage.title == "Login Error"
//                    && (result.throwable as NCLException).errorMessage.error ==
//                    "We couldn't find your login details.  Please try again."
//        }
    }
//
//    @Test
//    fun `test get login lookup success`() = runBlocking {
//
//        mockResponse.setResponseCode(HttpURLConnection.HTTP_OK)
//            .addHeader("content-type: application/json; charset=utf-8")
//            .setBody(successMessageBody)
//
//        mockWebServer.enqueue(mockResponse)
//
//        val result = remoteDataSource.onboardLookupLogin(mock())
//
//        assertTrue {
//            result is Resource.Success && result.data.clientId == "9823678"
//        }
//    }
//
//    @Test
//    fun `test get login lookup failure 500 error`() = runBlocking {
//
//        mockResponse.setResponseCode(HttpURLConnection.HTTP_INTERNAL_ERROR)
//
//        mockWebServer.enqueue(mockResponse)
//
//        val result = remoteDataSource.onboardLookupLogin(mock())
//
//        assertTrue {
//            result is Resource.Error && (result.throwable as NCLException).code == 500
//                    && (result.throwable as NCLException).errorCodeMap == NCLException.Code.NETWORK_ERROR
//                    && (result.throwable as NCLException).errorMessage == ErrorMessage("Server Error")
//
//        }
//    }
//
//    @Test
//    fun `test get login lookup failure Network Timeout`() = runBlocking {
//
//        mockResponse
//            .setBody("{}")
//            .setBodyDelay(1, TimeUnit.SECONDS)
//
//        mockWebServer.enqueue(mockResponse)
//
//        val result = remoteDataSource.onboardLookupLogin(mock())
//
//        assertTrue {
//            result is Resource.Error &&
//                    (result.throwable as NCLException).errorCodeMap == NCLException.Code.NETWORK_ERROR
//                    && (result.throwable as NCLException).errorMessage == ErrorMessage("timeout")
//
//        }
//    }
}

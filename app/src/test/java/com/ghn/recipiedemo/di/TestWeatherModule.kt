package com.ghn.recipiedemo.di

import com.ghn.recipiedemo.shared.extensions.createRetrofit
import com.ghn.recipiedemo.weather.data.api.WeatherApi
import com.ghn.recipiedemo.weather.data.remote.WeatherRemoteDataSourceImpl
import com.ghn.recipiedemo.weather.data.repository.WeatherRepositoryImpl
import com.ghn.recipiedemo.weather.domain.FetchWeatherUseCase
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val testWeatherModule = module {

    single(named("testWeatherAPI")) { provideTestWeatherAPI(get(named("testWeatherRetrofit"))) }

    single(named("remoteDataSourceFake")) {
        WeatherRemoteDataSourceImpl(get(named("testWeatherAPI")))
    }

    single(named("weatherRepositoryTestFakes")) {
        WeatherRepositoryImpl(
            get(named("remoteDataSourceFake"))
        )
    }

    factory(named("FetchWeatherUseCaseTest")) {
        FetchWeatherUseCase(
            get(named("weatherRepositoryTestFakes")),
            get(named("testCoroutineDispatcher"))
        )
    }
}

fun provideTestWeatherAPI(retrofit: Retrofit): WeatherApi = retrofit.createRetrofit()

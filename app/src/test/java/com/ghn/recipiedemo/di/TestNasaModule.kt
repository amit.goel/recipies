package com.ghn.recipiedemo.di

import com.ghn.recipiedemo.nasa.domain.GetNasaImageUseCaseFlow
import com.ghn.recipiedemo.nasa.source.api.NasaApi
import com.ghn.recipiedemo.nasa.source.remote.NasaRemoteDataSourceImpl
import com.ghn.recipiedemo.nasa.source.repository.NasaRepositoryImpl
import com.ghn.recipiedemo.shared.extensions.createRetrofit
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val testNasaModule = module {

    single(named("testNasaAPI")) { provideTestNasaAPI(get(named("testRetrofit"))) }

    single(named("remoteDataSourceFake")) {
        NasaRemoteDataSourceImpl(get(named("testNasaAPI")))
    }

    single(named("nasaRepositoryTestFakes")) {
        NasaRepositoryImpl(get(named("remoteDataSourceFake")))
    }

    factory(named("GetNasaInformationTest")) {
        GetNasaImageUseCaseFlow(
            get(named("nasaRepositoryTestFakes")),
            get(named("testCoroutineDispatcher"))
        )
    }
//
//    viewModel {
//        OnboardLookupReservationViewModel(
//            getOnboardLookupInformation = get(named("GetOnboardLookupInformationTest"))
//        )
//    }
}

fun provideTestNasaAPI(retrofit: Retrofit): NasaApi = retrofit.createRetrofit()

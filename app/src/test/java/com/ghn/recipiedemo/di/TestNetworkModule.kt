package com.ghn.recipiedemo.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockWebServer
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
val testNetworkModule = module {

    single {
        MockWebServer()
    }

    single(named("testCoroutineDispatcher")) {
        TestCoroutineDispatcher()
    }

    single(named("testOkHttpClient")) {
        OkHttpClient.Builder()
            .connectTimeout(500, TimeUnit.MILLISECONDS)
            .writeTimeout(500, TimeUnit.MILLISECONDS)
            .readTimeout(500, TimeUnit.MILLISECONDS)
            .addInterceptor(HttpLoggingInterceptor())
            .build()
    }

    single(named("testRetrofit")) {
        provideTestRetrofit(
            mockWebServer = get(),
            okHttpClient = get(named("testOkHttpClient"))
        )
    }

    single(named("testWeatherRetrofit")) {
        provideTestWeatherRetrofit(
            mockWebServer = get(),
            okHttpClient = get(named("testOkHttpClient"))
        )
    }
}

fun provideTestRetrofit(
    mockWebServer: MockWebServer,
    okHttpClient: OkHttpClient
): Retrofit {
    val factory =
        Json(JsonConfiguration(strictMode = false)).asConverterFactory("application/json".toMediaType())

    return Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .addConverterFactory(factory)
        .client(okHttpClient)
        .build()
}

fun provideTestWeatherRetrofit(
    mockWebServer: MockWebServer,
    okHttpClient: OkHttpClient
): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create())
        .baseUrl(mockWebServer.url("/"))
        .client(okHttpClient)
        .build()
}

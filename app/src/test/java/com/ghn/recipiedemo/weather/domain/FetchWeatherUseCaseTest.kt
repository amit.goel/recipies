package com.ghn.recipiedemo.weather.domain

import com.ghn.recipiedemo.di.testNetworkModule
import com.ghn.recipiedemo.di.testWeatherModule
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.qualifier.named
import org.koin.test.KoinTest
import org.koin.test.inject
import java.net.HttpURLConnection

@ExperimentalCoroutinesApi
class FetchWeatherUseCaseTest : KoinTest {

    private val mockWebServer by inject<MockWebServer>()
    private val testDispatcher by inject<TestCoroutineDispatcher>(
        named("testCoroutineDispatcher")
    )

    private val fetchWeatherUseCase by inject<FetchWeatherUseCase>(
        named("FetchWeatherUseCaseTest")
    )

    private lateinit var mockResponse: MockResponse

    private val successMessageBody: String = """
        {
            "data": [
                {
                    "moonrise_ts": 1581382478,
                    "wind_cdir": "E",
                    "rh": 77,
                    "pres": 1021.86,
                    "high_temp": 77.1,
                    "sunset_ts": 1581376300,
                    "ozone": 265.928,
                    "moon_phase": 0.84762,
                    "wind_gust_spd": 26.9,
                    "snow_depth": 0,
                    "clouds": 61,
                    "ts": 1581310860,
                    "sunrise_ts": 1581335823,
                    "app_min_temp": 73,
                    "wind_spd": 17.7,
                    "pop": 25,
                    "wind_cdir_full": "east",
                    "slp": 1022.56,
                    "valid_date": "2020-02-10",
                    "app_max_temp": 78,
                    "vis": 15,
                    "dewpt": 67.1,
                    "snow": 0,
                    "uv": 2.22147,
                    "weather": {
                        "icon": "c03d",
                        "code": 803,
                        "description": "Broken clouds"
                    },
                    "wind_dir": 97,
                    "max_dhi": null,
                    "clouds_hi": 0,
                    "precip": 0.042,
                    "low_temp": 73.9,
                    "max_temp": 77.5,
                    "moonset_ts": 1581345311,
                    "datetime": "2020-02-10",
                    "temp": 74.8,
                    "min_temp": 72.2,
                    "clouds_mid": 0,
                    "clouds_low": 60
                },
                {
                    "moonrise_ts": 1581472801,
                    "wind_cdir": "ESE",
                    "rh": 77,
                    "pres": 1019.08,
                    "high_temp": 79.9,
                    "sunset_ts": 1581462739,
                    "ozone": 271.57,
                    "moon_phase": 0.751051,
                    "wind_gust_spd": 19.5,
                    "snow_depth": 0,
                    "clouds": 31,
                    "ts": 1581397260,
                    "sunrise_ts": 1581422184,
                    "app_min_temp": 75,
                    "wind_spd": 11.5,
                    "pop": 20,
                    "wind_cdir_full": "east-southeast",
                    "slp": 1019.71,
                    "valid_date": "2020-02-11",
                    "app_max_temp": 80.8,
                    "vis": 15,
                    "dewpt": 69.1,
                    "snow": 0,
                    "uv": 6.0241,
                    "weather": {
                        "icon": "c02d",
                        "code": 802,
                        "description": "Scattered clouds"
                    },
                    "wind_dir": 122,
                    "max_dhi": null,
                    "clouds_hi": 17,
                    "precip": 0.005,
                    "low_temp": 75.1,
                    "max_temp": 80.5,
                    "moonset_ts": 1581434124,
                    "datetime": "2020-02-11",
                    "temp": 76.9,
                    "min_temp": 73.9,
                    "clouds_mid": 0,
                    "clouds_low": 17
                },
                {
                    "moonrise_ts": 1581563034,
                    "wind_cdir": "SE",
                    "rh": 76,
                    "pres": 1017.2,
                    "high_temp": 79.1,
                    "sunset_ts": 1581549177,
                    "ozone": 266.142,
                    "moon_phase": 0.642097,
                    "wind_gust_spd": 24,
                    "snow_depth": 0,
                    "clouds": 22,
                    "ts": 1581483660,
                    "sunrise_ts": 1581508544,
                    "app_min_temp": 76.5,
                    "wind_spd": 14,
                    "pop": 20,
                    "wind_cdir_full": "southeast",
                    "slp": 1017.88,
                    "valid_date": "2020-02-12",
                    "app_max_temp": 79.9,
                    "vis": 15,
                    "dewpt": 69.1,
                    "snow": 0,
                    "uv": 5.32139,
                    "weather": {
                        "icon": "c02d",
                        "code": 802,
                        "description": "Scattered clouds"
                    },
                    "wind_dir": 137,
                    "max_dhi": null,
                    "clouds_hi": 22,
                    "precip": 0.007,
                    "low_temp": 75.4,
                    "max_temp": 79.5,
                    "moonset_ts": 1581522953,
                    "datetime": "2020-02-12",
                    "temp": 77.2,
                    "min_temp": 75.1,
                    "clouds_mid": 0,
                    "clouds_low": 0
                },
                {
                    "moonrise_ts": 1581653197,
                    "wind_cdir": "SSE",
                    "rh": 76,
                    "pres": 1016.43,
                    "high_temp": 80.9,
                    "sunset_ts": 1581635615,
                    "ozone": 259.583,
                    "moon_phase": 0.527969,
                    "wind_gust_spd": 24,
                    "snow_depth": 0,
                    "clouds": 22,
                    "ts": 1581570060,
                    "sunrise_ts": 1581594903,
                    "app_min_temp": 76.9,
                    "wind_spd": 13.2,
                    "pop": 25,
                    "wind_cdir_full": "south-southeast",
                    "slp": 1017.07,
                    "valid_date": "2020-02-13",
                    "app_max_temp": 84.1,
                    "vis": 15,
                    "dewpt": 69.5,
                    "snow": 0,
                    "uv": 5.30566,
                    "weather": {
                        "icon": "c02d",
                        "code": 802,
                        "description": "Scattered clouds"
                    },
                    "wind_dir": 164,
                    "max_dhi": null,
                    "clouds_hi": 0,
                    "precip": 0.034,
                    "low_temp": 74,
                    "max_temp": 81.6,
                    "moonset_ts": 1581611871,
                    "datetime": "2020-02-13",
                    "temp": 77.9,
                    "min_temp": 75.5,
                    "clouds_mid": 0,
                    "clouds_low": 22
                },
                {
                    "moonrise_ts": 1581656916,
                    "wind_cdir": "SE",
                    "rh": 75,
                    "pres": 1018.42,
                    "high_temp": 79.7,
                    "sunset_ts": 1581722052,
                    "ozone": 257.436,
                    "moon_phase": 0.415248,
                    "wind_gust_spd": 21.7,
                    "snow_depth": 0,
                    "clouds": 7,
                    "ts": 1581656460,
                    "sunrise_ts": 1581681261,
                    "app_min_temp": 74.9,
                    "wind_spd": 8.1,
                    "pop": 20,
                    "wind_cdir_full": "southeast",
                    "slp": 1019.14,
                    "valid_date": "2020-02-14",
                    "app_max_temp": 80.3,
                    "vis": 15,
                    "dewpt": 67.9,
                    "snow": 0,
                    "uv": 7.12601,
                    "weather": {
                        "icon": "c02d",
                        "code": 801,
                        "description": "Few clouds"
                    },
                    "wind_dir": 146,
                    "max_dhi": null,
                    "clouds_hi": 6,
                    "precip": 0.005,
                    "low_temp": 73,
                    "max_temp": 80.3,
                    "moonset_ts": 1581700938,
                    "datetime": "2020-02-14",
                    "temp": 76.6,
                    "min_temp": 73.6,
                    "clouds_mid": 0,
                    "clouds_low": 3
                },
                {
                    "moonrise_ts": 1581746995,
                    "wind_cdir": "ENE",
                    "rh": 77,
                    "pres": 1022.32,
                    "high_temp": 77,
                    "sunset_ts": 1581808490,
                    "ozone": 261.537,
                    "moon_phase": 0.415248,
                    "wind_gust_spd": 30,
                    "snow_depth": 0,
                    "clouds": 84,
                    "ts": 1581742860,
                    "sunrise_ts": 1581767617,
                    "app_min_temp": 73.3,
                    "wind_spd": 22.2,
                    "pop": 75,
                    "wind_cdir_full": "east-northeast",
                    "slp": 1023,
                    "valid_date": "2020-02-15",
                    "app_max_temp": 75.5,
                    "vis": 12.7,
                    "dewpt": 66.3,
                    "snow": 0,
                    "uv": 2.71868,
                    "weather": {
                        "icon": "t02d",
                        "code": 201,
                        "description": "Thunderstorm with rain"
                    },
                    "wind_dir": 71,
                    "max_dhi": null,
                    "clouds_hi": 13,
                    "precip": 0.349,
                    "low_temp": 72.1,
                    "max_temp": 75.5,
                    "moonset_ts": 1581787338,
                    "datetime": "2020-02-15",
                    "temp": 73.9,
                    "min_temp": 72.1,
                    "clouds_mid": 0,
                    "clouds_low": 77
                },
                {
                    "moonrise_ts": 1581837010,
                    "wind_cdir": "E",
                    "rh": 76,
                    "pres": 1020.63,
                    "high_temp": 78.2,
                    "sunset_ts": 1581894926,
                    "ozone": 258.845,
                    "moon_phase": 0.309477,
                    "wind_gust_spd": 27.5,
                    "snow_depth": 0,
                    "clouds": 64,
                    "ts": 1581829260,
                    "sunrise_ts": 1581853973,
                    "app_min_temp": 72.7,
                    "wind_spd": 19.3,
                    "pop": 30,
                    "wind_cdir_full": "east",
                    "slp": 1021.21,
                    "valid_date": "2020-02-16",
                    "app_max_temp": 77.7,
                    "vis": 14.9,
                    "dewpt": 66.3,
                    "snow": 0,
                    "uv": 4.4028,
                    "weather": {
                        "icon": "c03d",
                        "code": 803,
                        "description": "Broken clouds"
                    },
                    "wind_dir": 97,
                    "max_dhi": null,
                    "clouds_hi": 5,
                    "precip": 0.054,
                    "low_temp": 74.3,
                    "max_temp": 77.6,
                    "moonset_ts": 1581876589,
                    "datetime": "2020-02-16",
                    "temp": 74.6,
                    "min_temp": 72.1,
                    "clouds_mid": 0,
                    "clouds_low": 61
                },
                {
                    "moonrise_ts": 1581926906,
                    "wind_cdir": "ESE",
                    "rh": 76,
                    "pres": 1018.69,
                    "high_temp": 77.6,
                    "sunset_ts": 1581981362,
                    "ozone": 258.033,
                    "moon_phase": 0.215074,
                    "wind_gust_spd": 21.5,
                    "snow_depth": 0,
                    "clouds": 19,
                    "ts": 1581915660,
                    "sunrise_ts": 1581940328,
                    "app_min_temp": 75.1,
                    "wind_spd": 16,
                    "pop": 10,
                    "wind_cdir_full": "east-southeast",
                    "slp": 1019.28,
                    "valid_date": "2020-02-17",
                    "app_max_temp": 79,
                    "vis": 15,
                    "dewpt": 67.9,
                    "snow": 0,
                    "uv": 7.21448,
                    "weather": {
                        "icon": "c02d",
                        "code": 801,
                        "description": "Few clouds"
                    },
                    "wind_dir": 107,
                    "max_dhi": null,
                    "clouds_hi": 2,
                    "precip": 0.015,
                    "low_temp": 74,
                    "max_temp": 78.4,
                    "moonset_ts": 1581966027,
                    "datetime": "2020-02-17",
                    "temp": 76,
                    "min_temp": 74.2,
                    "clouds_mid": 0,
                    "clouds_low": 18
                },
                {
                    "moonrise_ts": 1582016616,
                    "wind_cdir": "ESE",
                    "rh": 75,
                    "pres": 1018.48,
                    "high_temp": 77.8,
                    "sunset_ts": 1582067798,
                    "ozone": 260.566,
                    "moon_phase": 0.135413,
                    "wind_gust_spd": 20.9,
                    "snow_depth": 0,
                    "clouds": 45,
                    "ts": 1582002060,
                    "sunrise_ts": 1582026682,
                    "app_min_temp": 74.7,
                    "wind_spd": 15.9,
                    "pop": 10,
                    "wind_cdir_full": "east-southeast",
                    "slp": 1018.97,
                    "valid_date": "2020-02-18",
                    "app_max_temp": 78.3,
                    "vis": 15,
                    "dewpt": 66.8,
                    "snow": 0,
                    "uv": 5.33321,
                    "weather": {
                        "icon": "c03d",
                        "code": 803,
                        "description": "Broken clouds"
                    },
                    "wind_dir": 103,
                    "max_dhi": null,
                    "clouds_hi": 0,
                    "precip": 0.015,
                    "low_temp": 73.9,
                    "max_temp": 77.7,
                    "moonset_ts": 1582055618,
                    "datetime": "2020-02-18",
                    "temp": 75.4,
                    "min_temp": 73.9,
                    "clouds_mid": 0,
                    "clouds_low": 45
                },
                {
                    "moonrise_ts": 1582106081,
                    "wind_cdir": "ESE",
                    "rh": 72,
                    "pres": 1018.17,
                    "high_temp": 76.5,
                    "sunset_ts": 1582154234,
                    "ozone": 259.77,
                    "moon_phase": 0.0729814,
                    "wind_gust_spd": 19.3,
                    "snow_depth": 0,
                    "clouds": 58,
                    "ts": 1582088460,
                    "sunrise_ts": 1582113035,
                    "app_min_temp": 74.6,
                    "wind_spd": 12.4,
                    "pop": 25,
                    "wind_cdir_full": "east-southeast",
                    "slp": 1018.6,
                    "valid_date": "2020-02-19",
                    "app_max_temp": 78.3,
                    "vis": 14.9,
                    "dewpt": 65.8,
                    "snow": 0,
                    "uv": 5.63643,
                    "weather": {
                        "icon": "c03d",
                        "code": 803,
                        "description": "Broken clouds"
                    },
                    "wind_dir": 114,
                    "max_dhi": null,
                    "clouds_hi": 0,
                    "precip": 0.042,
                    "low_temp": 71.4,
                    "max_temp": 78.3,
                    "moonset_ts": 1582145295,
                    "datetime": "2020-02-19",
                    "temp": 75.5,
                    "min_temp": 73.7,
                    "clouds_mid": 0,
                    "clouds_low": 58
                },
                {
                    "moonrise_ts": 1582195270,
                    "wind_cdir": "SSE",
                    "rh": 77,
                    "pres": 1015.81,
                    "high_temp": 74.5,
                    "sunset_ts": 1582240669,
                    "ozone": 259.179,
                    "moon_phase": 0.0294862,
                    "wind_gust_spd": 14.6,
                    "snow_depth": 0,
                    "clouds": 30,
                    "ts": 1582174860,
                    "sunrise_ts": 1582199387,
                    "app_min_temp": 75.3,
                    "wind_spd": 10.8,
                    "pop": 20,
                    "wind_cdir_full": "south-southeast",
                    "slp": 1016.49,
                    "valid_date": "2020-02-20",
                    "app_max_temp": 77.5,
                    "vis": 15,
                    "dewpt": 67.8,
                    "snow": 0,
                    "uv": 1.45694,
                    "weather": {
                        "icon": "c02d",
                        "code": 802,
                        "description": "Scattered clouds"
                    },
                    "wind_dir": 157,
                    "max_dhi": null,
                    "clouds_hi": 11,
                    "precip": 0.032,
                    "low_temp": 63.2,
                    "max_temp": 79.6,
                    "moonset_ts": 1582234986,
                    "datetime": "2020-02-20",
                    "temp": 75.5,
                    "min_temp": 71.9,
                    "clouds_mid": 0,
                    "clouds_low": 22
                },
                {
                    "moonrise_ts": 1582284191,
                    "wind_cdir": "SSW",
                    "rh": 75,
                    "pres": 1016.05,
                    "high_temp": 67.5,
                    "sunset_ts": 1582327103,
                    "ozone": 263.715,
                    "moon_phase": 0.00590911,
                    "wind_gust_spd": 15.4,
                    "snow_depth": 0,
                    "clouds": 36,
                    "ts": 1582261260,
                    "sunrise_ts": 1582285738,
                    "app_min_temp": 72.6,
                    "wind_spd": 11,
                    "pop": 10,
                    "wind_cdir_full": "south-southwest",
                    "slp": 1016.68,
                    "valid_date": "2020-02-21",
                    "app_max_temp": 76.4,
                    "vis": 15,
                    "dewpt": 65.5,
                    "snow": 0,
                    "uv": 2.23998,
                    "weather": {
                        "icon": "c02d",
                        "code": 802,
                        "description": "Scattered clouds"
                    },
                    "wind_dir": 197,
                    "max_dhi": null,
                    "clouds_hi": 10,
                    "precip": 0.012,
                    "low_temp": 60.3,
                    "max_temp": 80,
                    "moonset_ts": 1582324631,
                    "datetime": "2020-02-21",
                    "temp": 73.8,
                    "min_temp": 71.9,
                    "clouds_mid": 0,
                    "clouds_low": 32
                },
                {
                    "moonrise_ts": 1582372875,
                    "wind_cdir": "SSE",
                    "rh": 76,
                    "pres": 1017.31,
                    "high_temp": 74.7,
                    "sunset_ts": 1582413537,
                    "ozone": 258.923,
                    "moon_phase": 0.00250799,
                    "wind_gust_spd": 9,
                    "snow_depth": 0,
                    "clouds": 79,
                    "ts": 1582347660,
                    "sunrise_ts": 1582372089,
                    "app_min_temp": 75.3,
                    "wind_spd": 5.4,
                    "pop": 60,
                    "wind_cdir_full": "south-southeast",
                    "slp": 1018.08,
                    "valid_date": "2020-02-22",
                    "app_max_temp": 76.9,
                    "vis": 12.9,
                    "dewpt": 67.1,
                    "snow": 0,
                    "uv": 1.31264,
                    "weather": {
                        "icon": "c04d",
                        "code": 804,
                        "description": "Overcast clouds"
                    },
                    "wind_dir": 165,
                    "max_dhi": null,
                    "clouds_hi": 0,
                    "precip": 0.172,
                    "low_temp": 70,
                    "max_temp": 78.8,
                    "moonset_ts": 1582414198,
                    "datetime": "2020-02-22",
                    "temp": 75.3,
                    "min_temp": 71.4,
                    "clouds_mid": 43,
                    "clouds_low": 66
                },
                {
                    "moonrise_ts": 1582461374,
                    "wind_cdir": "NW",
                    "rh": 67,
                    "pres": 1016.38,
                    "high_temp": 78.4,
                    "sunset_ts": 1582499971,
                    "ozone": 257.613,
                    "moon_phase": 0.0188189,
                    "wind_gust_spd": 12.8,
                    "snow_depth": 0,
                    "clouds": 46,
                    "ts": 1582434060,
                    "sunrise_ts": 1582458438,
                    "app_min_temp": 71.6,
                    "wind_spd": 8.4,
                    "pop": 0,
                    "wind_cdir_full": "northwest",
                    "slp": 1017.16,
                    "valid_date": "2020-02-23",
                    "app_max_temp": 74.6,
                    "vis": 15,
                    "dewpt": 61.3,
                    "snow": 0,
                    "uv": 2.18274,
                    "weather": {
                        "icon": "c03d",
                        "code": 803,
                        "description": "Broken clouds"
                    },
                    "wind_dir": 320,
                    "max_dhi": null,
                    "clouds_hi": 0,
                    "precip": 0,
                    "low_temp": 63.2,
                    "max_temp": 78.4,
                    "moonset_ts": 1582503683,
                    "datetime": "2020-02-23",
                    "temp": 72.9,
                    "min_temp": 63.2,
                    "clouds_mid": 1,
                    "clouds_low": 46
                },
                {
                    "moonrise_ts": 1582549743,
                    "wind_cdir": "S",
                    "rh": 44,
                    "pres": 1017.83,
                    "high_temp": 70.7,
                    "sunset_ts": 1582586404,
                    "ozone": 270.488,
                    "moon_phase": 0.0537135,
                    "wind_gust_spd": 22.7,
                    "snow_depth": 0,
                    "clouds": 32,
                    "ts": 1582520460,
                    "sunrise_ts": 1582544787,
                    "app_min_temp": 63.2,
                    "wind_spd": 14.4,
                    "pop": 0,
                    "wind_cdir_full": "south",
                    "slp": 1018.61,
                    "valid_date": "2020-02-24",
                    "app_max_temp": 65.6,
                    "vis": 15,
                    "dewpt": 42.1,
                    "snow": 0,
                    "uv": 1.25639,
                    "weather": {
                        "icon": "c02d",
                        "code": 802,
                        "description": "Scattered clouds"
                    },
                    "wind_dir": 188,
                    "max_dhi": null,
                    "clouds_hi": 0,
                    "precip": 0,
                    "low_temp": 60.2,
                    "max_temp": 70.7,
                    "moonset_ts": 1582593105,
                    "datetime": "2020-02-24",
                    "temp": 65.3,
                    "min_temp": 60.2,
                    "clouds_mid": 31,
                    "clouds_low": 8
                },
                {
                    "moonrise_ts": 1582638040,
                    "wind_cdir": "SSW",
                    "rh": 48,
                    "pres": 1019.72,
                    "high_temp": 72.3,
                    "sunset_ts": 1582672837,
                    "ozone": 279.966,
                    "moon_phase": 0.105542,
                    "wind_gust_spd": 15.5,
                    "snow_depth": 0,
                    "clouds": 22,
                    "ts": 1582606860,
                    "sunrise_ts": 1582631135,
                    "app_min_temp": 60.3,
                    "wind_spd": 10.6,
                    "pop": 0,
                    "wind_cdir_full": "south-southwest",
                    "slp": 1020.47,
                    "valid_date": "2020-02-25",
                    "app_max_temp": 69.1,
                    "vis": 15,
                    "dewpt": 44.9,
                    "snow": 0,
                    "uv": 2.20515,
                    "weather": {
                        "icon": "c02d",
                        "code": 802,
                        "description": "Scattered clouds"
                    },
                    "wind_dir": 200,
                    "max_dhi": null,
                    "clouds_hi": 0,
                    "precip": 0,
                    "low_temp": 69.4,
                    "max_temp": 72.3,
                    "moonset_ts": 1582682496,
                    "datetime": "2020-02-25",
                    "temp": 65.2,
                    "min_temp": 60.3,
                    "clouds_mid": 0,
                    "clouds_low": 22
                }
            ],
            "city_name": "Miami",
            "lon": "-80.19366",
            "timezone": "America/New_York",
            "lat": "25.77427",
            "country_code": "US",
            "state_code": "FL"
        }
	""".trimIndent()

    @Before
    fun setup() {
        startKoin {
            modules(listOf(testNetworkModule, testWeatherModule))
        }

        Dispatchers.setMain(testDispatcher)
        mockWebServer.start()

        mockResponse = MockResponse()
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()

        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        stopKoin()
    }

    @Test
    fun execute() = runBlocking {

        mockResponse.setResponseCode(HttpURLConnection.HTTP_OK)
            .addHeader("content-type: application/json; charset=utf-8")
            .setBody(successMessageBody)

        mockWebServer.enqueue(mockResponse)

        val result = fetchWeatherUseCase("Miami")
        assertTrue(result is Result.Success)
        assertTrue((result as Result.Success).data.cityName == "Miami")
        assertTrue(result.data.forecast.size == 16)
    }

    @Test
    fun `test repository network error`() = runBlocking {

        mockResponse.setResponseCode(HttpURLConnection.HTTP_GATEWAY_TIMEOUT)
            .addHeader("content-type: application/json; charset=utf-8")

        mockWebServer.enqueue(mockResponse)

        val result = fetchWeatherUseCase("Paris")
        assertTrue(result is Result.Error)
        assertTrue((result as Result.Error).throwable.message == "Remote Failure")
    }
}

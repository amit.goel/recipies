package com.ghn.recipiedemo.portfolio.data.models

import android.os.Parcelable
import com.ghn.recipiedemo.shared.util.SingleToArray
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Securities(
    @SingleToArray @field:Json(name = "security") val security: List<Security>
) : Parcelable

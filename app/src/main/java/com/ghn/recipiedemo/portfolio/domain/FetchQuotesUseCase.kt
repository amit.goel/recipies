package com.ghn.recipiedemo.portfolio.domain

import com.ghn.recipiedemo.portfolio.domain.repository.StocksRepository
import com.ghn.recipiedemo.shared.domain.UseCaseAlt
import kotlinx.coroutines.CoroutineDispatcher

open class FetchQuotesUseCase(
    private val stocksRepository: StocksRepository,
    defaultDispatcher: CoroutineDispatcher
) : UseCaseAlt(defaultDispatcher) {

    override suspend fun execute() {
        val quotes = stocksRepository.getQuotes()
        stocksRepository.fetchQuotes(quotes.joinToString(separator = ",") { it.symbol })
    }
}

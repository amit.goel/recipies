package com.ghn.recipiedemo.portfolio.data.api

import com.ghn.recipiedemo.BuildConfig
import com.ghn.recipiedemo.portfolio.data.models.SymbolResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SymbolsApi {

    @GET("stock/market/batch")
    suspend fun getQuotes(
        @Query("symbols") symbols: String,
        @Query("last") last: Int = 5,
        @Query("range") range: String = "1m",
        @Query("types") types: String = "quote",
        @Query("token") token: String = BuildConfig.SYMBOLS_API_KEY
    ): Response<Map<String, SymbolResponse>>
}

package com.ghn.recipiedemo.portfolio.presentation.portfolio

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.ghn.recipiedemo.databinding.FragmentPortfolioBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class PortfolioFragment : Fragment() {

    private val viewModel by viewModel<PortfolioViewModel>()

    private var _binding: FragmentPortfolioBinding? = null
    private val binding get() = _binding!!

    private val adapter by lazy {
        PortfolioAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPortfolioBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.stocksRv.adapter = adapter

        viewModel.quotes.observe(viewLifecycleOwner) { result ->
            adapter.submitList(result)
        }

        viewModel.getQuotesFromApi()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {

        fun newInstance() = PortfolioFragment()
    }
}

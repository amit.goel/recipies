package com.ghn.recipiedemo.portfolio.domain

import com.ghn.recipiedemo.portfolio.data.models.SymbolSearchResponse
import com.ghn.recipiedemo.portfolio.domain.repository.StocksRepository
import com.ghn.recipiedemo.shared.domain.SuspendUseCase
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.CoroutineDispatcher

class SearchSymbolsUseCase(
    private val stocksRepository: StocksRepository,
    defaultDispatcher: CoroutineDispatcher
) : SuspendUseCase<String, SymbolSearchResponse>(defaultDispatcher) {

    override suspend fun execute(parameters: String): SymbolSearchResponse {
        return when (val result = stocksRepository.searchSymbols(parameters)) {
            is Result.Success -> result.data
            is Result.Error -> throw result.throwable
            else -> throw IllegalStateException()
        }
    }
}

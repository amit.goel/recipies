package com.ghn.recipiedemo.portfolio.data.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class New(
    @field:Json(name = "datetime") val datetime: Long,
    @field:Json(name = "headline") val headline: String,
    @field:Json(name = "source") val source: String,
    @field:Json(name = "url") val url: String,
    @field:Json(name = "summary") val summary: String,
    @field:Json(name = "related") val related: String,
    @field:Json(name = "image") val image: String,
    @field:Json(name = "lang") val lang: String,
    @field:Json(name = "hasPaywall") val hasPaywall: Boolean
) : Parcelable

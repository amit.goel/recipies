package com.ghn.recipiedemo.portfolio.domain

import com.ghn.recipiedemo.portfolio.data.models.QuoteModule
import com.ghn.recipiedemo.portfolio.domain.repository.StocksRepository
import com.ghn.recipiedemo.shared.domain.FlowUseCaseAlt
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow

open class GetQuotesUseCaseFlow(
    private val stocksRepository: StocksRepository,
    defaultDispatcher: CoroutineDispatcher
) : FlowUseCaseAlt<String, List<QuoteModule>>(defaultDispatcher) {

    override fun execute(parameters: String): Flow<List<QuoteModule>> =
        stocksRepository.getQuotesFlow()
}

package com.ghn.recipiedemo.portfolio.data.base

import com.ghn.recipiedemo.portfolio.data.models.QuoteModule
import kotlinx.coroutines.flow.Flow

interface StocksLocalDataSource : StocksDataSource {

    suspend fun getQuoteBySymbol(symbol: String): QuoteModule?

    fun getQuotesFlow(): Flow<List<QuoteModule>>

    suspend fun getQuotes(): List<QuoteModule>

    suspend fun insertQuote(quote: QuoteModule)

    suspend fun updateQuote(quote: QuoteModule)
}

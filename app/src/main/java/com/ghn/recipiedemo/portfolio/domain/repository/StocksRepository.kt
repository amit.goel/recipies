package com.ghn.recipiedemo.portfolio.domain.repository

import com.ghn.recipiedemo.portfolio.data.models.QuoteModule
import com.ghn.recipiedemo.portfolio.data.models.Security
import com.ghn.recipiedemo.portfolio.data.models.SymbolSearchResponse
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.flow.Flow

interface StocksRepository {

    fun getQuotesFlow(): Flow<List<QuoteModule>>

    suspend fun getQuotes(): List<QuoteModule>

    suspend fun fetchQuotes(symbols: String)

    suspend fun searchSymbols(query: String): Result<SymbolSearchResponse>

    suspend fun addToWatchlist(security: Security): Result<Boolean>
}

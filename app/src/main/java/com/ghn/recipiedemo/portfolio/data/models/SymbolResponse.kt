package com.ghn.recipiedemo.portfolio.data.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SymbolResponse(
    @field:Json(name = "news") val news: List<New>,
    @field:Json(name = "quote") val quote: Quote
) : Parcelable

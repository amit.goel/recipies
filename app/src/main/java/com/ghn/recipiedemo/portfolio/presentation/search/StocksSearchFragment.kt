package com.ghn.recipiedemo.portfolio.presentation.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import com.ghn.recipiedemo.databinding.FragmentStocksSearchBinding
import com.ghn.recipiedemo.portfolio.data.models.Security
import com.ghn.recipiedemo.portfolio.data.models.SymbolSearchResponse
import com.ghn.recipiedemo.shared.extensions.hide
import com.ghn.recipiedemo.shared.extensions.show
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class StocksSearchFragment : Fragment() {

    private var _binding: FragmentStocksSearchBinding? = null

    private val binding get() = _binding!!

    private val viewModel by viewModel<StockSearchViewModel>()

    private val adapter by lazy {
        StocksSearchAdapter(addStockListener())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentStocksSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.stocksRv.adapter = adapter

        viewModel.query.value = ""

        binding.search.doOnTextChanged { text, _, _, _ ->
            viewModel.query.value = text.toString()
        }

        viewModel.symbols.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Success -> renderView(result.data)
//                is Result.Error -> handleError()
            }
        }
    }

    private fun renderView(data: SymbolSearchResponse) {
//        binding.swipeRefresh.isEnabled = true

        viewLifecycleOwner.lifecycleScope.launch {
            delay(150)
//            binding.swipeRefresh.isRefreshing = false

            if (!binding.stocksRv.isVisible) {
                binding.noStocksSearchData.hide()
                binding.stocksRv.show()
            }
        }

        adapter.submitList(data.securities?.security ?: listOf())
    }

    private fun addStockListener(): (Security) -> Unit {
        return { security: Security ->
            viewModel.addToWatchList(security)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {

        fun newInstance() =
            StocksSearchFragment()
    }
}

package com.ghn.recipiedemo.portfolio.data.base

import com.ghn.recipiedemo.portfolio.data.models.SymbolResponse
import com.ghn.recipiedemo.portfolio.data.models.SymbolSearchResponse
import com.ghn.recipiedemo.shared.util.Result

interface StocksRemoteDataSource : StocksDataSource {

    suspend fun getQuotes(symbols: String): Result<Map<String, SymbolResponse>>

    suspend fun searchSymbols(query: String): Result<SymbolSearchResponse>
}

package com.ghn.recipiedemo.portfolio.presentation.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ghn.recipiedemo.databinding.ItemSecuritySearchBinding
import com.ghn.recipiedemo.portfolio.data.models.Security

class StocksSearchAdapter(
    private val listener: (Security) -> Unit
) : ListAdapter<Security, StocksSearchAdapter.ViewHolder>(DiffCallback()) {

    private class DiffCallback : DiffUtil.ItemCallback<Security>() {
        override fun areItemsTheSame(oldItem: Security, newItem: Security): Boolean =
            oldItem.symbol == newItem.symbol

        override fun areContentsTheSame(oldItem: Security, newItem: Security): Boolean =
            oldItem == newItem
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemSecuritySearchBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(itemBinding)
    }

    inner class ViewHolder(private val viewBinding: ItemSecuritySearchBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(item: Security) {
            with(viewBinding) {
                symbol.text = item.symbol
                name.text = item.description

                addToListButton.setOnClickListener {
                    listener(item)
                }
            }
        }
    }
}

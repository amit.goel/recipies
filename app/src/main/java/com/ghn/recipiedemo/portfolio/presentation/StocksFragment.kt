package com.ghn.recipiedemo.portfolio.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.databinding.FragmentStocksBinding
import com.ghn.recipiedemo.portfolio.presentation.portfolio.PortfolioFragment
import com.ghn.recipiedemo.portfolio.presentation.search.StocksSearchFragment

class StocksFragment : Fragment() {

    private var _binding: FragmentStocksBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentStocksBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_settings3 -> {
                    val transaction = this@StocksFragment.parentFragmentManager.beginTransaction()
                    transaction.replace(R.id.container_fragment, PortfolioFragment.newInstance())
                    transaction.commit()
                }
                R.id.news_search -> {
                    val transaction = this@StocksFragment.parentFragmentManager.beginTransaction()
                    transaction.replace(
                        R.id.container_fragment,
                        StocksSearchFragment.newInstance()
                    )
                    transaction.commit()
                }
                R.id.action_sources -> {
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}

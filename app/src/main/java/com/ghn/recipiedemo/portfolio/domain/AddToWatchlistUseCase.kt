package com.ghn.recipiedemo.portfolio.domain

import com.ghn.recipiedemo.portfolio.data.models.Security
import com.ghn.recipiedemo.portfolio.domain.repository.StocksRepository
import com.ghn.recipiedemo.shared.domain.SuspendUseCase
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.CoroutineDispatcher

class AddToWatchlistUseCase(
    private val stocksRepository: StocksRepository,
    defaultDispatcher: CoroutineDispatcher
) : SuspendUseCase<Security, Boolean>(defaultDispatcher) {

    override suspend fun execute(parameters: Security): Boolean {
        return when (val result = stocksRepository.addToWatchlist(parameters)) {
            is Result.Success -> result.data
            is Result.Error -> throw result.throwable
            else -> throw IllegalStateException()
        }
    }
}

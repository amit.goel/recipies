package com.ghn.recipiedemo.portfolio.presentation.portfolio

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ghn.recipiedemo.databinding.ItemStockBinding
import com.ghn.recipiedemo.portfolio.data.models.QuoteModule

class PortfolioAdapter : ListAdapter<QuoteModule, PortfolioAdapter.ViewHolder>(
    DiffCallback()
) {

    private class DiffCallback : DiffUtil.ItemCallback<QuoteModule>() {
        override fun areItemsTheSame(oldItem: QuoteModule, newItem: QuoteModule): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: QuoteModule, newItem: QuoteModule): Boolean =
            oldItem == newItem
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemStockBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(itemBinding)
    }

    inner class ViewHolder(private val viewBinding: ItemStockBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(item: QuoteModule) {
            with(viewBinding) {
                symbol.text = item.symbol
                name.text = item.companyName
                price.text = String.format("$%.2f", item.latestPrice)

                val color = if ((item.change ?: 0.1) < 0.0) {
                    android.R.color.holo_red_dark
                } else {
                    android.R.color.holo_green_dark
                }

                price.setTextColor(ContextCompat.getColor(root.context, color))
            }
        }
    }
}

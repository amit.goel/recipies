package com.ghn.recipiedemo.portfolio.data.local

import com.ghn.recipiedemo.data.AppDatabase
import com.ghn.recipiedemo.portfolio.data.base.StocksLocalDataSource
import com.ghn.recipiedemo.portfolio.data.models.QuoteModule
import kotlinx.coroutines.flow.Flow

class StocksLocalDataSourceImpl(
    private val appDatabase: AppDatabase
) : StocksLocalDataSource {
    override suspend fun getQuoteBySymbol(symbol: String): QuoteModule? {
        return appDatabase.quotesDao().getSymbol(symbol)
    }

    override fun getQuotesFlow(): Flow<List<QuoteModule>> {
        return appDatabase.quotesDao().getAllFlow()
    }

    override suspend fun getQuotes(): List<QuoteModule> {
        return appDatabase.quotesDao().getAll()
    }

    override suspend fun insertQuote(quote: QuoteModule) {
        appDatabase.quotesDao().insert(quote)
    }

    override suspend fun updateQuote(quote: QuoteModule) {
        appDatabase.quotesDao().update(quote)
    }
}

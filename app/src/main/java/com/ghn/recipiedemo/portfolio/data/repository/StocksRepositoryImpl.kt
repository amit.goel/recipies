package com.ghn.recipiedemo.portfolio.data.repository

import com.ghn.recipiedemo.portfolio.data.base.StocksLocalDataSource
import com.ghn.recipiedemo.portfolio.data.base.StocksRemoteDataSource
import com.ghn.recipiedemo.portfolio.data.models.QuoteModule
import com.ghn.recipiedemo.portfolio.data.models.Security
import com.ghn.recipiedemo.portfolio.data.models.SymbolResponse
import com.ghn.recipiedemo.portfolio.data.models.SymbolSearchResponse
import com.ghn.recipiedemo.portfolio.domain.repository.StocksRepository
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow

class StocksRepositoryImpl(
    private val remoteDataSource: StocksRemoteDataSource,
    private val localDataSource: StocksLocalDataSource
) : StocksRepository {

    @InternalCoroutinesApi
    @ExperimentalCoroutinesApi
    override fun getQuotesFlow(): Flow<List<QuoteModule>> =
        localDataSource.getQuotesFlow()

    override suspend fun getQuotes(): List<QuoteModule> = localDataSource.getQuotes()

    override suspend fun fetchQuotes(symbols: String) {

        while (true) {
            val result = remoteDataSource.getQuotes(symbols)
            if (result is Result.Success) {
                result.data.forEach { (k, v: SymbolResponse) ->
                    val exists = localDataSource.getQuoteBySymbol(k)
                    val quote = v.quote.mapToQuoteModule()

                    if (exists != null) {
                        quote.id = exists.id
                        localDataSource.updateQuote(quote)
                    } else {
                        localDataSource.insertQuote(quote)
                    }
                }
            }
            delay(15000)
        }
    }

    override suspend fun searchSymbols(query: String): Result<SymbolSearchResponse> =
        remoteDataSource.searchSymbols(query)

    override suspend fun addToWatchlist(security: Security): Result<Boolean> {
        val exists = localDataSource.getQuoteBySymbol(security.symbol)

        val quote = QuoteModule(
            symbol = security.symbol,
            companyName = security.description
        )

        if (exists != null) {
            quote.id = exists.id
            localDataSource.updateQuote(quote)
        } else {
            localDataSource.insertQuote(quote)
        }

        return Result.Success(true)
    }
}

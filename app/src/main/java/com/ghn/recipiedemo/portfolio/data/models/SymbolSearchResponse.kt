package com.ghn.recipiedemo.portfolio.data.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SymbolSearchResponse(
    @field:Json(name = "securities") val securities: Securities?
) : Parcelable

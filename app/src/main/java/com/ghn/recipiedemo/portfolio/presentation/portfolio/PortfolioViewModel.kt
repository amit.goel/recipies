package com.ghn.recipiedemo.portfolio.presentation.portfolio

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.ghn.recipiedemo.portfolio.domain.FetchQuotesUseCase
import com.ghn.recipiedemo.portfolio.domain.GetQuotesUseCaseFlow
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

class PortfolioViewModel(
    private val getQuotesUseCaseFlow: GetQuotesUseCaseFlow,
    private val fetchQuotesUseCase: FetchQuotesUseCase
) : ViewModel() {

    @ExperimentalCoroutinesApi
    val quotes = getQuotesUseCaseFlow("").asLiveData()

    fun getQuotesFromApi() {
        viewModelScope.launch {
            fetchQuotesUseCase()
        }
    }
}

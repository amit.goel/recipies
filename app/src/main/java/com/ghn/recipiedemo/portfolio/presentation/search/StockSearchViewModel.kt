package com.ghn.recipiedemo.portfolio.presentation.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.ghn.recipiedemo.portfolio.data.models.Security
import com.ghn.recipiedemo.portfolio.domain.AddToWatchlistUseCase
import com.ghn.recipiedemo.portfolio.domain.SearchSymbolsUseCase
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class StockSearchViewModel(
    private val searchSymbolsUseCase: SearchSymbolsUseCase,
    private val addToWatchlistUseCase: AddToWatchlistUseCase
) : ViewModel() {

    fun addToWatchList(security: Security) {
        viewModelScope.launch {
            addToWatchlistUseCase(security)
        }
    }

    val query = MutableLiveData<String>()

    @FlowPreview
    @ExperimentalCoroutinesApi
    val symbols = query.asFlow()
        .debounce(250)
        .map { it.trim() }
        .onStart { Result.Loading }
        .flatMapLatest {
            flowOf(searchSymbolsUseCase(it))
        }
        .asLiveData()
}

package com.ghn.recipiedemo.portfolio.data.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Security(
    @field:Json(name = "symbol") val symbol: String,
    @field:Json(name = "exchange") val exchange: String,
    @field:Json(name = "type") val type: String,
    @field:Json(name = "description") val description: String
) : Parcelable

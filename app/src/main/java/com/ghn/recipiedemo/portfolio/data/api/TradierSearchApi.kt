package com.ghn.recipiedemo.portfolio.data.api

import com.ghn.recipiedemo.BuildConfig
import com.ghn.recipiedemo.portfolio.data.models.SymbolSearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface TradierSearchApi {

    @Headers(
        "Accept: application/json",
        "Authorization: Bearer ${BuildConfig.TRADIER_API_KEY}"
    )
    @GET("markets/lookup")
    suspend fun searchSymbols(
        @Query("q") query: String,
        @Query("types") types: String = "stock",
        @Query("exchanges") exchanges: String = "Q,N"
    ): Response<SymbolSearchResponse>
}

package com.ghn.recipiedemo.portfolio.data.remote

import com.ghn.recipiedemo.portfolio.data.api.SymbolsApi
import com.ghn.recipiedemo.portfolio.data.api.TradierSearchApi
import com.ghn.recipiedemo.portfolio.data.base.StocksRemoteDataSource
import com.ghn.recipiedemo.portfolio.data.models.SymbolResponse
import com.ghn.recipiedemo.portfolio.data.models.SymbolSearchResponse
import com.ghn.recipiedemo.shared.extensions.handleResult
import com.ghn.recipiedemo.shared.util.Result

class StocksRemoteDataSourceImpl(
    private val api: SymbolsApi,
    private val tradierSearchApi: TradierSearchApi
) : StocksRemoteDataSource {

    override suspend fun getQuotes(symbols: String): Result<Map<String, SymbolResponse>> {

        val result = api.getQuotes(symbols)
        return if (result.isSuccessful) {
            result.body()?.let { body ->
                Result.Success(body)
            } ?: Result.Error(java.lang.Exception("Expecting body"))
        } else {
            Result.Error(java.lang.Exception("Remote Failure"))
        }
    }

    override suspend fun searchSymbols(query: String): Result<SymbolSearchResponse> =
        tradierSearchApi.searchSymbols(query).handleResult()
}

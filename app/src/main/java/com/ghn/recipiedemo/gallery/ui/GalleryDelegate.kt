package com.ghn.recipiedemo.gallery.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.gallery.data.model.response.Category
import com.ghn.recipiedemo.shared.adapter.ViewType
import com.ghn.recipiedemo.shared.adapter.ViewTypeDelegateAdapter
import com.ghn.recipiedemo.shared.extensions.inflate
import com.ghn.recipiedemo.shared.extensions.loadImage
import kotlinx.android.synthetic.main.item_gallery.view.*

class GalleryDelegate(
    itemListener: (String, Boolean) -> Unit
) : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup) = GalleryViewHolder(parent)

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: ViewType,
        payloads: MutableList<Any>
    ) {
        (holder as GalleryViewHolder).bind(item as Category)
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {}

    override fun onViewDetached(holder: RecyclerView.ViewHolder) {}

    inner class GalleryViewHolder(val parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_gallery)) {

        fun bind(item: Category) {
            with(itemView) {
                category_name.text = item.name
                category_description.text = item.description

                gallery_image.loadImage(item.thumbnail!!)
            }
        }
    }
}

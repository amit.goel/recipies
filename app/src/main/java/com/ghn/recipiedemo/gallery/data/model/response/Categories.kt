package com.ghn.recipiedemo.gallery.data.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Categories(
    @SerialName("categories") val categories: List<Category>?
) : Parcelable

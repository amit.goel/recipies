package com.ghn.recipiedemo.gallery.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.gallery.data.model.response.Categories
import com.ghn.recipiedemo.gallery.data.source.base.GalleryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class GalleryViewModel(
    private val repository: GalleryRepository
) : ViewModel() {

    private val _meals = MutableLiveData<Result<Categories>>()

    val meals
        get() = _meals

    fun getMeals() {
        viewModelScope.launch(Dispatchers.IO) {
            val result = repository.getMeals()
            meals.postValue(result)
        }
    }
}

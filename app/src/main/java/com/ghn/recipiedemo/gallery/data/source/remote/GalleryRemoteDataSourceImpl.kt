package com.ghn.recipiedemo.gallery.data.source.remote

import com.ghn.recipiedemo.gallery.data.model.response.Categories
import com.ghn.recipiedemo.gallery.data.source.api.RecipeAPI
import com.ghn.recipiedemo.gallery.data.source.base.GalleryRemoteDataSource
import com.ghn.recipiedemo.shared.util.Result

class GalleryRemoteDataSourceImpl(
    private val api: RecipeAPI
) : GalleryRemoteDataSource {

    override suspend fun getMeals(): Result<Categories> {
        val result = api.getMeals()

        if (result.isSuccessful) {
            result.body()?.let { body ->
                return Result.Success(body)
            }
        }

        return Result.Error(Exception("No enough pastries!"))
    }
}

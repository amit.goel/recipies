package com.ghn.recipiedemo.gallery.data.source.api

import com.ghn.recipiedemo.gallery.data.model.response.Categories
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface RecipeAPI {

    @GET("{id}/categories.php")
    suspend fun getMeals(@Path("id") id: Int = 1): Response<Categories>
}

package com.ghn.recipiedemo.gallery.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.gallery.data.model.response.Categories
import kotlinx.android.synthetic.main.fragment_gallery.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class GalleryFragment : Fragment() {

    private var adapter: GalleryAdapter? = null

    private val viewModel: GalleryViewModel by viewModel()

    private var next: String? = null
    private var last: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_gallery, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObservers()

        if (adapter == null) {
            adapter = GalleryAdapter(galleryListener())
        }

        if (gallery_rv.adapter == null) {
            configureAdapter()
        }

        viewModel.getMeals()
    }

    private fun configureAdapter() {
        gallery_rv.apply {
            itemAnimator = null
            adapter = this@GalleryFragment.adapter
        }
    }

    private fun setUpObservers() {
        viewModel.meals.observe(this) { result ->
            when (result) {
                is Result.Success -> renderView(result.data)
                is Result.Error -> handleError(result.throwable)
            }
        }
    }

    private fun renderView(data: Categories) {
        data.categories?.forEach { cat ->
            Timber.d("Category: ${cat.name}: ${cat.description}")
        }

        data.categories?.let { adapter?.addItems(it.toMutableList()) }
    }

    private fun handleError(throwable: Throwable) {
        Timber.d(throwable)
    }

    private fun galleryListener(): (String, Boolean) -> Unit {
        return { _: String, _: Boolean -> }
    }
}

package com.ghn.recipiedemo.gallery.data.source.base

import com.ghn.recipiedemo.gallery.data.model.response.Categories
import com.ghn.recipiedemo.shared.util.Result

interface GalleryDataSource {

    suspend fun getMeals(): Result<Categories>
}

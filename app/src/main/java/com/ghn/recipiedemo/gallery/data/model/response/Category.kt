package com.ghn.recipiedemo.gallery.data.model.response

import android.os.Parcelable
import com.ghn.recipiedemo.shared.adapter.AdapterConstants
import com.ghn.recipiedemo.shared.adapter.ViewType
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Category(
    @SerialName("idCategory") val id: String?,
    @SerialName("strCategory") val name: String?,
    @SerialName("strCategoryThumb") val thumbnail: String?,
    @SerialName("strCategoryDescription") val description: String?
) : Parcelable, ViewType {

    override fun getViewType(): Int = AdapterConstants.CATEGORY
}

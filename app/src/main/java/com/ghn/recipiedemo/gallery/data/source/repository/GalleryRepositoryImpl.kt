package com.ghn.recipiedemo.gallery.data.source.repository

import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.gallery.data.model.response.Categories
import com.ghn.recipiedemo.gallery.data.source.base.GalleryRemoteDataSource
import com.ghn.recipiedemo.gallery.data.source.base.GalleryRepository

class GalleryRepositoryImpl(
    private val remoteDataSource: GalleryRemoteDataSource
) : GalleryRepository {

    override suspend fun getMeals(): Result<Categories> {
        return remoteDataSource.getMeals()
    }
}

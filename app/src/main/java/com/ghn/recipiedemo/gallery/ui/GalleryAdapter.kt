package com.ghn.recipiedemo.gallery.ui

import android.util.SparseArray
import com.ghn.recipiedemo.shared.adapter.AdapterConstants
import com.ghn.recipiedemo.shared.adapter.BaseAdapter
import com.ghn.recipiedemo.shared.adapter.ViewType
import com.ghn.recipiedemo.shared.adapter.ViewTypeDelegateAdapter

class GalleryAdapter(
    private val itemListener: (String, Boolean) -> Unit
) : BaseAdapter() {

    override fun setupDelegateAdapters(delegateAdapters: SparseArray<ViewTypeDelegateAdapter>) {
        delegateAdapters.apply {
            put(AdapterConstants.CATEGORY, GalleryDelegate(itemListener))
        }
    }

    override fun addItems(itemViews: MutableList<ViewType>) {

        if (items.lastOrNull()?.getViewType() == AdapterConstants.LOADING) {
            items.removeAt(itemCount - 1)
            notifyItemRemoved(itemCount)
        }

        val count = itemCount
        items.addAll(itemViews)
        notifyItemRangeInserted(count + 1, itemViews.size)
    }
}

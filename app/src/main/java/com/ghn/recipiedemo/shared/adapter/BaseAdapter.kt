package com.ghn.recipiedemo.shared.adapter

import android.util.SparseArray
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber

private const val DELEGATE_EXCEPTION_MESSAGE =
    "Delegate adapter to parse the specified view type is missing. Have you added it to the delegate adapter list in the child class?"

abstract class BaseAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    protected var items: MutableList<ViewType> = arrayListOf()
    protected var animationEnabled: Boolean = true
    private var lastPosition = -1

    protected val delegateAdapters: SparseArray<ViewTypeDelegateAdapter> by lazy {
        SparseArray<ViewTypeDelegateAdapter>().apply {
            //            put(AdapterConstants.NO_CONTENT, NoContentDelegateAdapter())
            setupDelegateAdapters(this)
        }
    }

    protected abstract fun setupDelegateAdapters(
        delegateAdapters: SparseArray<ViewTypeDelegateAdapter>
    )

    abstract fun addItems(itemViews: MutableList<ViewType>)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = run {
        val delegateAdapter = delegateAdapters.get(viewType)
            ?: throw IllegalStateException(DELEGATE_EXCEPTION_MESSAGE)
        delegateAdapter.onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) = holder.bindView(position, payloads)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        holder.bindView(position, mutableListOf())

    private fun RecyclerView.ViewHolder.bindView(position: Int, payloads: MutableList<Any>) {
        delegateAdapters.get(getItemViewType(position))
            .onBindViewHolder(this, items[position], payloads)

        if (adapterPosition > lastPosition) {
//            if (animationEnabled) {
//                val animationType: Int = when (this@BaseAdapter) {
//                    is BadgeAdapter -> R.anim.zoom_in
//                    is NotificationBaseAdapter -> R.anim.fade_out_new
//                    is MessageAdapter -> R.anim.image_zoom_in
//                    else -> /*R.anim.fade_out_new*/  R.anim.item_animation_fall_down
//                }
//                val animation = AnimationUtils.loadAnimation(itemView.context, animationType)
//                itemView.startAnimation(animation)
//            }

            lastPosition = position
        }
    }

    override fun getItemViewType(position: Int) = items[position].getViewType()

    override fun getItemCount(): Int = items.size

    override fun getItemId(position: Int): Long = position.toLong()

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        Timber.i("onViewRecycled")
        delegateAdapters.get(holder.itemViewType).onViewRecycled(holder)
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        holder.itemView.clearAnimation()

        super.onViewDetachedFromWindow(holder)
        Timber.i("onViewDetachedFromWindow")
        delegateAdapters.get(holder.itemViewType).onViewDetached(holder)
    }

    protected val loadingItem = object : ViewType {
        override fun getViewType() = AdapterConstants.LOADING
    }

    open val hasContentItems: Boolean
        get() = items.size > 1 || (items.size == 1 &&
                items[0].getViewType() != AdapterConstants.LOADING)

    fun addLoading(front: Boolean = false) {
        if (!front) {
            items.add(loadingItem)
            notifyItemInserted(itemCount)
        } else {
            items.add(0, loadingItem)
            notifyItemInserted(0)
        }
    }

    fun clearItems() {
        items.clear()
        lastPosition = -1
        notifyDataSetChanged()
    }

    protected fun reset() {
        lastPosition = -1
    }

    fun updateItem(item: ViewType, payload: Any? = null) {
        /*items.firstOrNull {
            (it.getViewType() == AdapterConstants.POST && (it as PostObject).idNumber == itemId)
                    || (it.getViewType() == AdapterConstants.COMMENT && (it as CommentObject).idNumber == itemId)
        } ?: return*/

        val index = items.indexOf(item)
        if (index != -1) {
            notifyItemChanged(index, payload)
        }
    }

    fun getPosition(item: ViewType): Int = items.indexOf(item)

    fun removeItem(item: ViewType) {
        val index = items.indexOf(item)
        if (index != -1) {
            items.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    fun getItem(position: Int): ViewType {
        return items[position]
    }
}

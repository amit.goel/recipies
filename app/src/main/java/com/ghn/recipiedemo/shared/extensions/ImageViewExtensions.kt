package com.ghn.recipiedemo.shared.extensions

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.ghn.recipiedemo.GlideApp

fun ImageView.loadImage(
    url: String,
    listener: RequestListener<Drawable>? = null
) {
    GlideApp.with(context)
        .load(url)
        .listener(listener)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}

fun ImageView.getImageFromURL(
    url: String?,
    options: RequestOptions = RequestOptions().dontTransform(),
    listener: RequestListener<Drawable>? = null
) {
    GlideApp.with(context)
        .load(url)
//            .thumbnail(0.4f)
        .listener(listener)
        .transition(DrawableTransitionOptions.withCrossFade())
        // .transition(GenericTransitionOptions.with(R.anim.image_zoom_in))
        .apply(options)
        .into(this)
}

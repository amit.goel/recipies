package com.ghn.recipiedemo.shared.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

interface ViewTypeDelegateAdapter {

    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: ViewType,
        payloads: MutableList<Any>
    )

    fun onViewRecycled(holder: RecyclerView.ViewHolder)

    fun onViewDetached(holder: RecyclerView.ViewHolder)
}

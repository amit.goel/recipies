package com.ghn.recipiedemo.shared.util

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.text.TextUtils
import androidx.browser.customtabs.CustomTabsService

class CustomTabHelper {

    companion object {
        var packageNameToUse: String? = null
        const val STABLE_PACKAGE = "com.android.chrome"
        const val BETA_PACKAGE = "com.chrome.beta"
        const val DEV_PACKAGE = "com.chrome.dev"
        const val LOCAL_PACKAGE = "com.google.android.apps.chrome"
    }

    fun getPackageNameToUse(context: Context, url: String): String? {

        if (packageNameToUse != null) return packageNameToUse

        val pm = context.packageManager

        val activityIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        val defaultViewHandlerInfo = pm.resolveActivity(activityIntent, 0)
        var defaultViewHandlerPackageName: String? = null

        defaultViewHandlerInfo?.let {
            defaultViewHandlerPackageName = it.activityInfo.packageName
        }

        val resolvedActivityList = pm.queryIntentActivities(activityIntent, 0)
        val packagesSupportingCustomTabs = ArrayList<String>()
        resolvedActivityList.forEach { info ->
            val serviceIntent = Intent()
            serviceIntent.action = CustomTabsService.ACTION_CUSTOM_TABS_CONNECTION
            serviceIntent.setPackage(info.activityInfo.packageName)

            pm.resolveService(serviceIntent, 0)?.let {
                packagesSupportingCustomTabs.add(info.activityInfo.packageName)
            }
        }

        with(packagesSupportingCustomTabs) {
            when {
                isEmpty() -> packageNameToUse = null
                size == 1 -> packageNameToUse = get(0)
                !TextUtils.isEmpty(defaultViewHandlerPackageName) &&
                        !hasSpecializedHandlerIntents(context, activityIntent) &&
                        contains(defaultViewHandlerPackageName) ->
                    packageNameToUse = defaultViewHandlerPackageName
                contains(STABLE_PACKAGE) -> packageNameToUse = STABLE_PACKAGE
                contains(BETA_PACKAGE) -> packageNameToUse = BETA_PACKAGE
                contains(DEV_PACKAGE) -> packageNameToUse = DEV_PACKAGE
                contains(LOCAL_PACKAGE) -> packageNameToUse = LOCAL_PACKAGE
            }
        }
        return packageNameToUse
    }

    private fun hasSpecializedHandlerIntents(context: Context, intent: Intent): Boolean {
        try {
            val handlers = context.packageManager.queryIntentActivities(
                intent,
                PackageManager.GET_RESOLVED_FILTER
            )

            if (handlers == null || handlers.size == 0)
                return false

            handlers.forEach { resolveInfo ->
                val filter = resolveInfo.filter ?: return@forEach
                if (filter.countDataAuthorities() == 0 || filter.countDataPaths() == 0)
                    return@forEach

                if (resolveInfo.activityInfo == null) return@forEach
                return true
            }
        } catch (e: RuntimeException) {
        }
        return false
    }
}

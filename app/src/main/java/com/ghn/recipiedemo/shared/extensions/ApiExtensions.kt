package com.ghn.recipiedemo.shared.extensions

import com.ghn.recipiedemo.shared.util.Result
import retrofit2.Response
import retrofit2.Retrofit

inline fun <reified T> Retrofit.createRetrofit(): T = this.create(T::class.java)

// inline fun <reified T> Gson.extractFromJson(body: String?): T = this.fromJson(body, T::class.java)

fun <T> Response<T>.handleResult(): Result<T> = when {
    isSuccessful -> body()?.let { body ->
        Result.Success(body)
    } ?: Result.Error(Exception("Expecting body"))
    else -> Result.Error(Exception("Remote Failure"))
}

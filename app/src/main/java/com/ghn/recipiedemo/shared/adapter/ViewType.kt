package com.ghn.recipiedemo.shared.adapter

interface ViewType {
    fun getViewType(): Int
}

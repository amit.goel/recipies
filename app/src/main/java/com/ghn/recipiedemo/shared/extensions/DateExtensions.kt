package com.ghn.recipiedemo.shared.extensions

import java.text.SimpleDateFormat
import java.util.*

val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)

fun Date.getLastTenDays(): List<String> {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = time

    return (0..9).map {
        calendar.add(Calendar.DAY_OF_YEAR, if (it == 0) 0 else -1)
        sdf.format(calendar.time)
    }
}

fun Date.subtractTenDays(): Date {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = time

    calendar.add(Calendar.DAY_OF_YEAR, -10)
    return calendar.time
}

fun String.formatDate(): String {
    val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val formatter = SimpleDateFormat("EEE, MMM dd", Locale.getDefault())

    val date: Date? = format.parse(this)
    return formatter.format(date)
}

package com.ghn.recipiedemo.shared.adapter

object AdapterConstants {
    private var BASE_COUNT = 0

    val LOADING = BASE_COUNT++
    val CATEGORY = BASE_COUNT++
    val NASA = BASE_COUNT++
    val NASA_IMAGE = BASE_COUNT++
    val NEWS = BASE_COUNT++
}

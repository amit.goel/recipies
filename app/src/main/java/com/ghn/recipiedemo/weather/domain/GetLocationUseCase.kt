package com.ghn.recipiedemo.weather.domain

import com.ghn.recipiedemo.shared.domain.SuspendUseCase
import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.models.Location
import com.ghn.recipiedemo.weather.domain.repository.LocationRepository
import kotlinx.coroutines.CoroutineDispatcher

class GetLocationUseCase(
    private val locationRepository: LocationRepository,
    defaultDispatcher: CoroutineDispatcher
) : SuspendUseCase<String, Location>(defaultDispatcher) {

    override suspend fun execute(parameters: String): Location {
        return when (val result = locationRepository.getLocationData()) {
            is Result.Success -> result.data
            is Result.Error -> throw result.throwable
            else -> throw IllegalStateException()
        }
    }
}

package com.ghn.recipiedemo.weather

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ghn.recipiedemo.GlideApp
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.databinding.ItemForecastBinding
import com.ghn.recipiedemo.shared.extensions.formatDate
import com.ghn.recipiedemo.weather.data.models.response.DaysForecast

class WeatherAdapter : ListAdapter<DaysForecast, WeatherAdapter.ViewHolder>(DiffCallback()) {

    private class DiffCallback : DiffUtil.ItemCallback<DaysForecast>() {
        override fun areItemsTheSame(oldItem: DaysForecast, newItem: DaysForecast): Boolean =
            oldItem.datetime + oldItem.temp == newItem.datetime + newItem.temp

        override fun areContentsTheSame(oldItem: DaysForecast, newItem: DaysForecast): Boolean =
            oldItem == newItem
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemForecastBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(itemBinding)
    }

    inner class ViewHolder(private val viewBinding: ItemForecastBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(item: DaysForecast) {
            with(viewBinding) {
                val context = root.context

                date.text = item.datetime.formatDate()

                temperature.text =
                    context.getString(R.string.temperature_formatter, item.temp.toString())

                pressure.text =
                    context.getString(R.string.pressure_formatter, item.pressure.toString())

                humidity.text = context.getString(
                    R.string.humidity_formatter,
                    item.relativeHumidity.toString()
                )

                precipitation.text = context.getString(
                    R.string.precipitation_formatter,
                    item.chancePrecipitation.toString()
                )

                condition.text = item.weather.description

                GlideApp.with(weatherIcon).load("$IMAGE_URL${item.weather.icon}.png")
                    .into(weatherIcon)
            }
        }
    }

    companion object {
        const val IMAGE_URL = "https://www.weatherbit.io/static/img/icons/"
    }
}

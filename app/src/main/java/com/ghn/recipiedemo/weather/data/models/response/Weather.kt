package com.ghn.recipiedemo.weather.data.models.response

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Weather(
    @field:Json(name = "code") val code: Int,
    @field:Json(name = "description") val description: String,
    @field:Json(name = "icon") val icon: String
) : Parcelable

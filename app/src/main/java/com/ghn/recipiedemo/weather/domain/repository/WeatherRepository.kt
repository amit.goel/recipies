package com.ghn.recipiedemo.weather.domain.repository

import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.models.response.WeatherResponse

interface WeatherRepository {

    suspend fun getForecast(location: String): Result<WeatherResponse>
}

package com.ghn.recipiedemo.weather.data.base

import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.models.Location

interface LocationDataSource {
    suspend fun getLocationData(): Result<Location>
}

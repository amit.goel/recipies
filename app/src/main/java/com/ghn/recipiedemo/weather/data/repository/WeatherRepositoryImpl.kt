package com.ghn.recipiedemo.weather.data.repository

import com.ghn.recipiedemo.shared.util.Result

import com.ghn.recipiedemo.weather.data.base.WeatherRemoteDataSource
import com.ghn.recipiedemo.weather.data.models.response.WeatherResponse
import com.ghn.recipiedemo.weather.domain.repository.WeatherRepository

class WeatherRepositoryImpl(
    private val remoteDataSource: WeatherRemoteDataSource
) : WeatherRepository {

    override suspend fun getForecast(location: String): Result<WeatherResponse> {
        return remoteDataSource.getForecast(location)
    }
}

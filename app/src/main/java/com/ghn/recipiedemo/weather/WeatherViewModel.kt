package com.ghn.recipiedemo.weather

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.models.Location
import com.ghn.recipiedemo.weather.domain.FetchWeatherUseCase
import com.ghn.recipiedemo.weather.domain.GetLocationUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class WeatherViewModel(
    private val fetchWeatherUseCase: FetchWeatherUseCase,
    private val getLocationUseCase: GetLocationUseCase
) : ViewModel() {

    val location = MutableLiveData<String>()

    private val _locationData = MutableLiveData<Result<Location>>()
    val locationData = _locationData

    fun getUserLocation() {
        viewModelScope.launch {
            val result = getLocationUseCase("")
            _locationData.postValue(result)
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    val forecasts = location.asFlow()
        .debounce(250)
        .map { it.trim() }
        .flatMapLatest {
            flowOf(fetchWeatherUseCase(it))
        }
        .asLiveData()
}

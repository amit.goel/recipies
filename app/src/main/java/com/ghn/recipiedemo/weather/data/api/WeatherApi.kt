package com.ghn.recipiedemo.weather.data.api

import com.ghn.recipiedemo.BuildConfig
import com.ghn.recipiedemo.weather.data.models.response.WeatherResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("forecast/daily")
    suspend fun getForecast(
        @Query("city") city: String,
        @Query("key") key: String = BuildConfig.WEATHER_API_KEY,
        @Query("units") units: String = "I"
    ): Response<WeatherResponse>
}

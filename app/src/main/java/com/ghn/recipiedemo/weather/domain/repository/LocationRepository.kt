package com.ghn.recipiedemo.weather.domain.repository

import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.models.Location

interface LocationRepository {

    suspend fun getLocationData(): Result<Location>
}

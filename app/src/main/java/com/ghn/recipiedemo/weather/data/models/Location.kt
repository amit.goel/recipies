package com.ghn.recipiedemo.weather.data.models

data class Location(
    val latitude: Double,
    val longitude: Double,
    val city: String
)

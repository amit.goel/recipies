package com.ghn.recipiedemo.weather

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import com.ghn.recipiedemo.BuildConfig
import com.ghn.recipiedemo.GlideApp
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.databinding.FragmentWeatherBinding
import com.ghn.recipiedemo.shared.extensions.hide
import com.ghn.recipiedemo.shared.extensions.hideKeyboard
import com.ghn.recipiedemo.shared.extensions.show
import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.models.Location
import com.ghn.recipiedemo.weather.data.models.response.WeatherResponse
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
@FlowPreview
class WeatherFragment : Fragment() {

    private val viewModel by viewModel<WeatherViewModel>()

    private var _binding: FragmentWeatherBinding? = null
    private val binding get() = _binding!!

    private val weatherAdapter by lazy {
        WeatherAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        _binding = FragmentWeatherBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            forecastRv.adapter = weatherAdapter

//            swipeRefresh.isEnabled = false
//            swipeRefresh.setOnRefreshListener {
//                viewModel.location.value = binding.search.text.toString()
//            }

            mapFab.setOnClickListener {
                showMapLocation(search.text.toString())
            }

            searchGoBtn.setOnClickListener {
                hideKeyboard()

                when {
                    search.text.toString().isEmpty() -> {
                        if (forecastRv.isVisible) {
                            forecastRv.hide()
                            noWeatherData.show()
                        }

                        Toast.makeText(
                            requireContext(),
                            R.string.empty_search_error,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    else -> {
//                        swipeRefresh.isRefreshing = true
                        viewModel.location.value = search.text.toString()
                    }
                }
            }
        }

        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.forecasts.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Success -> renderView(result.data)
                is Result.Error -> handleError()
            }
        }

        viewModel.locationData.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Success -> handleLocationSuccess(result.data)
                is Result.Error -> handleLocationError(result.throwable)
            }
        }
    }

    private fun handleLocationError(throwable: Throwable) {
//        binding.swipeRefresh.isRefreshing = false
//        binding.swipeRefresh.isEnabled = false

        if (throwable.message == "GPSDisabled") {
            showEnableGPSAlert()
        }
        Toast.makeText(requireContext(), R.string.location_error, Toast.LENGTH_LONG).show()
    }

    private fun handleLocationSuccess(data: Location) {
//        binding.swipeRefresh.isRefreshing = true
        binding.search.setText(data.city)
        hideKeyboard()
        viewModel.location.value = data.city
    }

    private fun handleError() {
        binding.mapFab.hide()
//        binding.swipeRefresh.isRefreshing = false
//        binding.swipeRefresh.isEnabled = false

        binding.forecastRv.hide()
        binding.noWeatherData.show()

        Toast.makeText(requireContext(), R.string.weather_search_error, Toast.LENGTH_LONG).show()
    }

    private fun renderView(data: WeatherResponse) {
//        binding.swipeRefresh.isEnabled = true

        viewLifecycleOwner.lifecycleScope.launch {
            delay(150)
//            binding.swipeRefresh.isRefreshing = false

            if (!binding.forecastRv.isVisible) {
                binding.noWeatherData.hide()
                binding.forecastRv.show {
                    binding.mapFab.show()
                }
            }
        }

        weatherAdapter.submitList(data.forecast)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_weather, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_location) {
            setupPermissions()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        } else {
            viewModel.getUserLocation()
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {
        const val PERM_REQUEST_CODE = 123
    }

    private fun makeRequest() {
        requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERM_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERM_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    when {
                        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                            when (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                                true -> showAlert()
                                false -> showPermissionDeniedAlert()
                            }
                        }
                        else -> makeRequest()
                    }
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    viewModel.getUserLocation()
                }
            }
        }
    }

    private fun showAlert() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.location_permission_title)
            .setMessage(R.string.location_perm_reason)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok) { _, _ -> setupPermissions() }
            .show()
    }

    private fun showPermissionDeniedAlert() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.location_permission_title)
            .setMessage(R.string.location_perm_reason_denied)
            .setPositiveButton(android.R.string.ok) { _, _ -> enableLocationSettings() }
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun showEnableGPSAlert() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.enable_gps_title)
            .setMessage(R.string.gps_request_reason)
            .setPositiveButton(android.R.string.ok) { _, _ -> enableLocationSettings() }
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun enableLocationSettings() {
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivity(intent)
    }

    private fun showMapLocation(location: String) {
        val dialog = BottomSheetDialog(requireActivity())
        val sheetView = layoutInflater.inflate(R.layout.fragment_map_bottom_sheet, null)
        dialog.setContentView(sheetView)
        dialog.show()

        val imageMap = dialog.findViewById<ImageView>(R.id.map)

        if (imageMap != null) {
            val mapUrlInitial = "https://maps.googleapis.com/maps/api/staticmap?center="
            val mapUrlProperties = "&zoom=13&size=1200x400&markers=color:red%7C"
            val mapUrlMapType = "&key=${BuildConfig.MAPS_API_KEY}"

            val url = mapUrlInitial + location + mapUrlProperties + location + mapUrlMapType

            GlideApp.with(sheetView)
                .load(url)
                .placeholder(R.drawable.image_background)
                .into(imageMap)
        }
    }
}

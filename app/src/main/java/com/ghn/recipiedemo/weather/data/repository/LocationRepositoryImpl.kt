package com.ghn.recipiedemo.weather.data.repository

import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.base.LocationDataSource
import com.ghn.recipiedemo.weather.data.models.Location
import com.ghn.recipiedemo.weather.domain.repository.LocationRepository

class LocationRepositoryImpl(
    private val dataSource: LocationDataSource
) : LocationRepository {

    override suspend fun getLocationData(): Result<Location> {
        return dataSource.getLocationData()
    }
}

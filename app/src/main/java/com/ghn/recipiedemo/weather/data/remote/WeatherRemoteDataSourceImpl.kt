package com.ghn.recipiedemo.weather.data.remote

import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.api.WeatherApi
import com.ghn.recipiedemo.weather.data.base.WeatherRemoteDataSource
import com.ghn.recipiedemo.weather.data.models.response.WeatherResponse
import retrofit2.Response

class WeatherRemoteDataSourceImpl(
    private val api: WeatherApi
) : WeatherRemoteDataSource {

    override suspend fun getForecast(location: String): Result<WeatherResponse> {
        val result = api.getForecast(location)
        return result.handleResult()
    }

    private fun <T> Response<T>.handleResult(): Result<T> = when {
        isSuccessful -> body()?.let { body ->
            Result.Success(body)
        } ?: Result.Error(Exception("Expecting body"))
        else -> Result.Error(Exception("Remote Failure"))
    }
}

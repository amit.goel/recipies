package com.ghn.recipiedemo.weather.data.base

import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.models.response.WeatherResponse

interface WeatherDataSource {

    suspend fun getForecast(location: String): Result<WeatherResponse>
}

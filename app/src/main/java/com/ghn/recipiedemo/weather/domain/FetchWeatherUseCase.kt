package com.ghn.recipiedemo.weather.domain

import com.ghn.recipiedemo.shared.domain.SuspendUseCase
import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.models.response.WeatherResponse
import com.ghn.recipiedemo.weather.domain.repository.WeatherRepository
import kotlinx.coroutines.CoroutineDispatcher

class FetchWeatherUseCase(
    private val weatherRepository: WeatherRepository,
    defaultDispatcher: CoroutineDispatcher
) : SuspendUseCase<String, WeatherResponse>(defaultDispatcher) {

    override suspend fun execute(parameters: String): WeatherResponse {
        return when (val result = weatherRepository.getForecast(parameters)) {
            is Result.Success -> result.data
            is Result.Error -> throw result.throwable
            else -> throw IllegalStateException()
        }
    }
}

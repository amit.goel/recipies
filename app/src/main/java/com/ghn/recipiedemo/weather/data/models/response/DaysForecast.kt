package com.ghn.recipiedemo.weather.data.models.response

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Suppress("SpellCheckingInspection")
@Parcelize
data class DaysForecast(
    @field:Json(name = "app_max_temp") val apparentMaxTemp: Double,
    @field:Json(name = "app_min_temp") val apparentMinTemp: Double,
    @field:Json(name = "clouds") val clouds: Int,
    @field:Json(name = "clouds_hi") val cloudsHi: Int,
    @field:Json(name = "clouds_low") val cloudsLow: Int,
    @field:Json(name = "clouds_mid") val cloudsMid: Int,
    @field:Json(name = "datetime") val datetime: String,
    @field:Json(name = "dewpt") val dewPoint: Double,
    @field:Json(name = "high_temp") val highTemp: Double?,
    @field:Json(name = "low_temp") val lowTemp: Double?,
    @field:Json(name = "max_temp") val maxTemp: Double,
    @field:Json(name = "min_temp") val minTemp: Double,
    @field:Json(name = "moon_phase") val moonPhase: Double,
    @field:Json(name = "moonrise_ts") val moonriseTs: Int,
    @field:Json(name = "moonset_ts") val moonsetTs: Int,
    @field:Json(name = "ozone") val ozone: Double,
    @field:Json(name = "pop") val chancePrecipitation: Int,
    @field:Json(name = "precip") val precipitation: Double,
    @field:Json(name = "pres") val pressure: Double,
    @field:Json(name = "rh") val relativeHumidity: Int,
    @field:Json(name = "slp") val seaLevelPressure: Double,
    @field:Json(name = "snow") val snow: Double,
    @field:Json(name = "snow_depth") val snowDepth: Double,
    @field:Json(name = "sunrise_ts") val sunriseTs: Int,
    @field:Json(name = "sunset_ts") val sunsetTs: Int,
    @field:Json(name = "temp") val temp: Double,
    @field:Json(name = "ts") val timestamp: Int,
    @field:Json(name = "uv") val uv: Double,
    @field:Json(name = "valid_date") val validDate: String,
    @field:Json(name = "vis") val visibility: Double,
    @field:Json(name = "weather") val weather: Weather,
    @field:Json(name = "wind_cdir") val windCdir: String,
    @field:Json(name = "wind_cdir_full") val windCdirFull: String,
    @field:Json(name = "wind_dir") val windDir: Int,
    @field:Json(name = "wind_gust_spd") val windGustSpd: Double,
    @field:Json(name = "wind_spd") val windSpd: Double
) : Parcelable

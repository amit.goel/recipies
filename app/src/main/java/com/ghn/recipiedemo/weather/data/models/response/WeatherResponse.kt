package com.ghn.recipiedemo.weather.data.models.response

import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class WeatherResponse(
    @field:Json(name = "city_name") val cityName: String,
    @field:Json(name = "country_code") val countryCode: String,
    @field:Json(name = "data") val forecast: List<DaysForecast>,
    @field:Json(name = "lat") val lat: String,
    @field:Json(name = "lon") val lon: String,
    @field:Json(name = "state_code") val stateCode: String,
    @field:Json(name = "timezone") val timezone: String
) : Parcelable

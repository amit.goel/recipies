package com.ghn.recipiedemo.weather.data

import android.annotation.SuppressLint
import android.location.Address
import android.location.Criteria
import android.location.Geocoder
import android.location.LocationManager
import com.ghn.recipiedemo.shared.util.Result
import com.ghn.recipiedemo.weather.data.base.LocationDataSource
import com.ghn.recipiedemo.weather.data.models.Location
import java.io.IOException

class LocationDataSourceImpl(
    private val locationManager: LocationManager,
    private val geoCoder: Geocoder
) : LocationDataSource {

    @SuppressLint("MissingPermission")
    override suspend fun getLocationData(): Result<Location> {

        val criteria = Criteria().apply {
            accuracy = Criteria.ACCURACY_FINE
        }

        val gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

        if (!gpsEnabled) {
            return Result.Error(IOException("GPSDisabled"))
        }

        val providerName = locationManager.getBestProvider(criteria, true)

        if (providerName != null) {
            val location: android.location.Location? =
                locationManager.getLastKnownLocation(providerName)

            if (location != null) {
                val latitude = location.latitude
                val longitude = location.longitude

                val addresses: List<Address> = geoCoder.getFromLocation(latitude, longitude, 1)
                val cityName = addresses[0].locality
                // , ${addresses[0].adminArea}, ${addresses[0].countryName}

                val userLocation = Location(latitude, longitude, cityName)
                return Result.Success(userLocation)
            }
        }

        return Result.Error(IOException("Failed to get user's location"))
    }
}

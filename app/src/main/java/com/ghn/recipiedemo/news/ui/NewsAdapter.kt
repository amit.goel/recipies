package com.ghn.recipiedemo.news.ui

import android.util.SparseArray
import com.ghn.recipiedemo.shared.adapter.AdapterConstants
import com.ghn.recipiedemo.shared.adapter.BaseAdapter
import com.ghn.recipiedemo.shared.adapter.ViewType
import com.ghn.recipiedemo.shared.adapter.ViewTypeDelegateAdapter

class NewsAdapter(
    private val itemListener: (String) -> Unit
) : BaseAdapter() {

    override fun setupDelegateAdapters(delegateAdapters: SparseArray<ViewTypeDelegateAdapter>) {
        delegateAdapters.apply {
            put(AdapterConstants.NEWS, NewsDelegate(itemListener))
        }
    }

    override fun addItems(itemViews: MutableList<ViewType>) {
        val count = itemCount
        items.addAll(itemViews)
        notifyItemRangeInserted(count + 1, itemViews.size)
    }
}

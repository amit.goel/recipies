package com.ghn.recipiedemo.news.ui

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.VectorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.databinding.FragmentNewsBinding
import com.ghn.recipiedemo.news.data.models.response.News
import com.ghn.recipiedemo.shared.adapter.ViewType
import com.ghn.recipiedemo.shared.util.CustomTabHelper
import com.ghn.recipiedemo.shared.util.InfiniteScrollListener
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

@ExperimentalCoroutinesApi
class NewsFragment : Fragment() {

    private lateinit var binding: FragmentNewsBinding

    private var newsAdapter: NewsAdapter? = null
    private val viewModel: NewsViewModel by viewModel()
    private var endlessScrollListener: InfiniteScrollListener? = null

    private var pageCount = 1
    private var totalCount = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (newsAdapter == null) {
            newsAdapter = NewsAdapter(newsListener())
        }

        if (binding.newsRv.adapter == null) {
            configureAdapter()
        }

        viewModel.getNews(NewsMeta())
        viewModel.news.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> binding.swipeRefreshNews.isRefreshing = true
                is Result.Success -> {
                    binding.swipeRefreshNews.isRefreshing = false
                    renderView(result.data)
                }
                is Result.Error -> {
                    binding.swipeRefreshNews.isRefreshing = false
                    Timber.d(result.throwable)
                }
            }
        }

        viewModel.repo.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    newsAdapter?.clearItems()
                    binding.swipeRefreshNews.isRefreshing = true
                }
                is Result.Success -> {
                    binding.swipeRefreshNews.isRefreshing = false
                    renderView(result.data)
                }
                is Result.Error -> {
                    binding.swipeRefreshNews.isRefreshing = false
                    Timber.d(result.throwable)
                }
            }
//                adapter.submitList(it.data?.repositories)
        }

        binding.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                pageCount = 1
                viewModel.query.value = newText
                return true
            }
        })

        binding.bottomNavigation.getOrCreateBadge(R.id.action_settings2)?.number = 2
    }

    private fun renderView(data: News) {
        data.value?.forEach {
            Timber.d("Found: ${it.title} - ${it.url}")
        }

        pageCount += 1
        totalCount = data.totalCount ?: 0

        newsAdapter?.addItems(data.value?.toMutableList<ViewType>()!!)
    }

    private fun configureAdapter() {
        binding.newsRv.apply {
            //            itemAnimator = null
            adapter = newsAdapter

            binding.swipeRefreshNews.setOnRefreshListener {
                (adapter as? NewsAdapter)?.clearItems()
                viewModel.getNews(NewsMeta())
            }

            endlessScrollListener = object :
                InfiniteScrollListener(layoutManager as LinearLayoutManager) {
                override fun onLoadMore(page: Int, count: Int, view: RecyclerView) {
                    if (totalCount > 0 && (pageCount * 20) < totalCount) {
                        viewModel.getNews(
                            NewsMeta(pageCount.toString())
                        )
                    }
                }
            }

            endlessScrollListener?.let { addOnScrollListener(it) }
        }
    }

    private fun newsListener(): (String) -> Unit {
        return { url: String ->
            val builder = CustomTabsIntent.Builder()
                .setShowTitle(true)
                .setCloseButtonIcon(
                    getBitmapFromDrawable(requireContext(), R.drawable.ic_arrow_back_black_24dp)
                )
//                .setStartAnimations(activity, R.anim.fragment_enter_from_right, R.anim.fragment_exit_to_left)
//                .setExitAnimations(activity, android.R.anim.fade_in, android.R.anim.fade_out)

            val packageName = CustomTabHelper().getPackageNameToUse(requireActivity(), url)

            if (packageName == null)
//                with(NewsSiteFragment.newInstance(url)) {
//                    this@MainNavigator.activity.replaceFragment(this, "tag", addToBackStack = true)
//                }
            else {
                val customTabsIntent = builder.build()
                customTabsIntent.launchUrl(requireActivity(), Uri.parse(url))
            }
        }
    }

    private fun getBitmapFromDrawable(context: Context, drawableId: Int): Bitmap {
        return when (val drawable = ContextCompat.getDrawable(context, drawableId)) {
            is BitmapDrawable -> drawable.bitmap
            is VectorDrawable -> getBitmapFromVectorDrawable(drawable)
            else -> throw IllegalArgumentException("Unable to convert to bitmap")
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getBitmapFromVectorDrawable(vectorDrawable: VectorDrawable): Bitmap {
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.setTint(Color.WHITE)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
        vectorDrawable.draw(canvas)
        return bitmap
    }
}

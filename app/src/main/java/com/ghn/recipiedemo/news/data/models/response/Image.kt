package com.ghn.recipiedemo.news.data.models.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Image(
    @SerialName("url") val url: String?,
    @SerialName("height") val height: Int?,
    @SerialName("width") val width: Int?,
    @SerialName("thumbnail") val thumbnail: String?,
    @SerialName("thumbnailHeight") val thumbnailHeight: Int?,
    @SerialName("thumbnailWidth") val thumbnailWidth: Int?,
    @SerialName("base64Encoding") val base64Encoding: String?,
    @SerialName("name") val name: String?,
    @SerialName("title") val title: String?,
    @SerialName("imageWebSearchUrl") val imageWebSearchUrl: String?
) : Parcelable

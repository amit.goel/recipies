package com.ghn.recipiedemo.news.source.api

import com.ghn.recipiedemo.BuildConfig
import com.ghn.recipiedemo.news.data.models.response.News
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

private const val NEWS_HOST = "contextualwebsearch-websearch-v1.p.rapidapi.com"

interface NewsApi {

    @Headers(
        "x-rapidapi-host: $NEWS_HOST",
        "x-rapidapi-key: ${BuildConfig.NEWS_API_KEY}"
    )
    @GET("Search/NewsSearchAPI")
    suspend fun fetchNews(
        @Query("q") query: String = "amd",
        @Query("pageNumber") pageNumber: String = "1",
        @Query("pageSize") size: String = "20"
    ): Response<News>
}

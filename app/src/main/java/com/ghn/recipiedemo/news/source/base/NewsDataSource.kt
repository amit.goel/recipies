package com.ghn.recipiedemo.news.source.base

import com.ghn.recipiedemo.news.data.models.response.News
import com.ghn.recipiedemo.news.ui.NewsMeta
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.flow.Flow

interface NewsDataSource {
    fun getNews(meta: NewsMeta): Flow<Result<News>>
}

package com.ghn.recipiedemo.news.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.ghn.recipiedemo.news.data.models.response.News
import com.ghn.recipiedemo.news.domain.GetNewsUseCaseFlow
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class NewsViewModel(
    private val getNewsUseCaseFlow: GetNewsUseCaseFlow
) : ViewModel() {

    val query = MutableLiveData<String>()

    private val _news = MutableLiveData<Result<News>>()

    val news: LiveData<Result<News>> = _news

    fun getNews(meta: NewsMeta) {
        viewModelScope.launch {
            getNewsUseCaseFlow(meta).collect {
                _news.value = it
            }
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    val repo = query.asFlow()
        .debounce(300)
        .filter { it.trim().isNotEmpty() }
        .distinctUntilChanged()
        .flatMapLatest {
            getNewsUseCaseFlow(NewsMeta(query = it))
        }.asLiveData()
}

data class NewsMeta(
    val pageNumber: String = "1",
    val page_count: String = "20",
    val query: String = "trump"
)

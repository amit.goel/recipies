package com.ghn.recipiedemo.news.ui

import android.text.Html
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.news.data.models.response.Value
import com.ghn.recipiedemo.shared.adapter.ViewType
import com.ghn.recipiedemo.shared.adapter.ViewTypeDelegateAdapter
import com.ghn.recipiedemo.shared.extensions.inflate
import com.ghn.recipiedemo.shared.extensions.loadImage
import kotlinx.android.synthetic.main.item_news.view.*

class NewsDelegate(private val itemListener: (String) -> Unit) : ViewTypeDelegateAdapter {

    private val currentTime: Long by lazy {
        System.currentTimeMillis()
    }

    override fun onCreateViewHolder(parent: ViewGroup) = NewsViewHolder(parent)

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: ViewType,
        payloads: MutableList<Any>
    ) {
        (holder as NewsViewHolder).bind(item as Value)
    }

    inner class NewsViewHolder(val parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_news)) {

        fun bind(item: Value) {
            with(itemView) {
                news_image.loadImage(item.image?.url!!)

                news_image_title.text = Html.fromHtml(item.title)
//                news_description.text = item.body
                news_publisher.text = item.provider?.name

                setOnClickListener {
                    itemListener(item.url!!)
                }
            }
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {}
    override fun onViewDetached(holder: RecyclerView.ViewHolder) {}
}

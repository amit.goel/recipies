package com.ghn.recipiedemo.news.data.models.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class News(
    @SerialName("_type") val type: String?,
    @SerialName("didUMean") val didUMean: String?,
    @SerialName("totalCount") val totalCount: Int?,
    @SerialName("relatedSearch") val relatedSearch: List<String>?,
    @SerialName("value") val value: List<Value>?
) : Parcelable

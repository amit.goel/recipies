package com.ghn.recipiedemo.news.data.models.response

import android.os.Parcelable
import com.ghn.recipiedemo.shared.adapter.AdapterConstants
import com.ghn.recipiedemo.shared.adapter.ViewType
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Value(
    @SerialName("title") val title: String?,
    @SerialName("url") val url: String?,
    @SerialName("description") val description: String?,
    @SerialName("body") val body: String?,
    @SerialName("keywords") val keywords: String?,
    @SerialName("language") val language: String?,
    @SerialName("isSafe") val isSafe: Boolean?,
    @SerialName("datePublished") val datePublished: String?,
    @SerialName("provider") val provider: Provider?,
    @SerialName("image") val image: Image?
) : Parcelable, ViewType {
    override fun getViewType(): Int = AdapterConstants.NEWS
}

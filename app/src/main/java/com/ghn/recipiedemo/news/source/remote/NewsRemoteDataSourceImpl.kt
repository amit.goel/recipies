package com.ghn.recipiedemo.news.source.remote

import com.ghn.recipiedemo.news.data.models.response.News
import com.ghn.recipiedemo.news.source.api.NewsApi
import com.ghn.recipiedemo.news.source.base.NewsRemoteDataSource
import com.ghn.recipiedemo.news.ui.NewsMeta
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class NewsRemoteDataSourceImpl(
    private val newsApi: NewsApi
) : NewsRemoteDataSource {

    override fun getNews(meta: NewsMeta): Flow<Result<News>> = flow {
        if (meta.pageNumber == "1")
            emit(Result.Loading)

        val result = newsApi.fetchNews(query = meta.query, pageNumber = meta.pageNumber)
        if (result.isSuccessful) {
            result.body()?.let { body ->
                emit(Result.Success(body))
            } ?: Result.Error(java.lang.Exception("Expecting body"))
        } else {
            Result.Error(java.lang.Exception("Remote Failure"))
        }
    }
}

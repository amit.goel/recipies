package com.ghn.recipiedemo.news.domain

import com.ghn.recipiedemo.news.data.models.response.News
import com.ghn.recipiedemo.news.source.base.NewsRepository
import com.ghn.recipiedemo.news.ui.NewsMeta
import com.ghn.recipiedemo.shared.domain.FlowUseCase
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow

open class GetNewsUseCaseFlow(
    private val newsRepository: NewsRepository,
    defaultDispatcher: CoroutineDispatcher
) : FlowUseCase<NewsMeta, News>(defaultDispatcher) {

    override fun execute(parameters: NewsMeta): Flow<Result<News>> =
        newsRepository.getNews(parameters)
}

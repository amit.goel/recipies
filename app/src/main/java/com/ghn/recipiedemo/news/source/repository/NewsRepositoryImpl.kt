package com.ghn.recipiedemo.news.source.repository

import com.ghn.recipiedemo.news.data.models.response.News
import com.ghn.recipiedemo.news.source.base.NewsRemoteDataSource
import com.ghn.recipiedemo.news.source.base.NewsRepository
import com.ghn.recipiedemo.news.ui.NewsMeta
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.flow.Flow

class NewsRepositoryImpl(
    private val remoteDataSource: NewsRemoteDataSource
) : NewsRepository {

    override fun getNews(date: NewsMeta): Flow<Result<News>> {
        return remoteDataSource.getNews(date)
    }
}

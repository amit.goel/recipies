package com.ghn.recipiedemo.news.data.models.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Provider(
    @SerialName("name") val name: String?
) : Parcelable

package com.ghn.recipiedemo.nasa.source.remote

import com.ghn.recipiedemo.nasa.data.models.response.NasaImage
import com.ghn.recipiedemo.nasa.data.models.response.PictureOfTheDay
import com.ghn.recipiedemo.nasa.source.api.NasaApi
import com.ghn.recipiedemo.nasa.source.base.NasaRemoteDataSource
import com.ghn.recipiedemo.shared.extensions.getLastTenDays
import com.ghn.recipiedemo.shared.extensions.sdf
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class NasaRemoteDataSourceImpl(
    private val nasaAPI: NasaApi
) : NasaRemoteDataSource {

    override suspend fun getPictureOfTheDay(date: String): Result<PictureOfTheDay> {
        val result = nasaAPI.pictureOfTheDay(date)

        if (result.isSuccessful) {
            result.body()?.let { body ->
                return Result.Success(body)
            }
        }

        return Result.Error(Exception("Not enough planets!"))
    }

    override suspend fun getNasaImages(query: String): Result<NasaImage> {
        val result = nasaAPI.getImages()

        if (result.isSuccessful) {
            result.body()?.let { body ->
                return Result.Success(body)
            }
        }

        return Result.Error(Exception("Not enough Images!"))
    }

    override fun getPictureOfTheDayFlow(
        date: String
    ): Flow<Result<PictureOfTheDay>> {

        return flow {
            emit(Result.Loading)
            val convertedCurrentDate = sdf.parse(date)
            val lastTenDays = convertedCurrentDate?.getLastTenDays()

            lastTenDays?.forEach {
                val result = nasaAPI.pictureOfTheDay(it)

                if (result.isSuccessful) {
                    result.body()?.let { body ->
                        if (body.hdUrl != null)
                            emit(Result.Success(body))
                    } ?: Result.Error(java.lang.Exception("Expecting body"))
                } else {
                    Result.Error(java.lang.Exception("Remote Failure"))
                }
            }
        }
    }
}

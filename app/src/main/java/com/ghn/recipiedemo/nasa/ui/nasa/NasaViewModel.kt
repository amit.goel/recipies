package com.ghn.recipiedemo.nasa.ui.nasa

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ghn.recipiedemo.nasa.data.models.response.PictureOfTheDay
import com.ghn.recipiedemo.nasa.domain.GetNasaImageUseCase
import com.ghn.recipiedemo.nasa.domain.GetNasaImageUseCaseFlow
import com.ghn.recipiedemo.shared.extensions.subtractTenDays
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.Date

@ExperimentalCoroutinesApi
class NasaViewModel(
    private val nasaImageUseCase: GetNasaImageUseCase,
    private val nasaImageUseCaseFlow: GetNasaImageUseCaseFlow
) : ViewModel() {

    private var date: Date = Date()

    private val _picture = MutableLiveData<Result<PictureOfTheDay>>()
    private val _pictures = MutableLiveData<Result<PictureOfTheDay>>()

    val picture: LiveData<Result<PictureOfTheDay>> = _picture
    val pictures: LiveData<Result<PictureOfTheDay>> = _pictures

    fun getPicture() {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val currentDate = sdf.format(date)

        viewModelScope.launch {
            val result = nasaImageUseCase(currentDate)
            _picture.value = result
        }
    }

    fun getPictureFlow() {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val currentDate = sdf.format(date)

        viewModelScope.launch {
            nasaImageUseCaseFlow(currentDate).collect {
                _pictures.value = it
            }

            date = date.subtractTenDays()
        }
    }
}

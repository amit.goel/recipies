package com.ghn.recipiedemo.nasa.data.models.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Collection(
    @SerialName("version") val version: String? = null,
    @SerialName("links") val links: List<Link>? = null,
    @SerialName("items") val items: List<Item>? = null,
    @SerialName("metadata") val metadata: Metadata? = null,
    @SerialName("href") val href: String? = null
) : Parcelable

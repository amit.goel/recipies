package com.ghn.recipiedemo.nasa.source.base

import com.ghn.recipiedemo.nasa.data.models.response.NasaImage
import com.ghn.recipiedemo.nasa.data.models.response.PictureOfTheDay
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.flow.Flow

interface NasaDataSource {
    suspend fun getPictureOfTheDay(date: String): Result<PictureOfTheDay>

    suspend fun getNasaImages(query: String): Result<NasaImage>

    fun getPictureOfTheDayFlow(
        date: String
    ): Flow<Result<PictureOfTheDay>>
}

package com.ghn.recipiedemo.nasa.data.models.response

import android.os.Parcelable
import com.ghn.recipiedemo.shared.adapter.AdapterConstants
import com.ghn.recipiedemo.shared.adapter.ViewType
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Item(
    @SerialName("links") val links: List<ItemLink>? = null,
    @SerialName("data") val itemData: List<Data>? = null,
    @SerialName("href") val href: String? = null
) : Parcelable, ViewType {
    override fun getViewType(): Int = AdapterConstants.NASA_IMAGE
}

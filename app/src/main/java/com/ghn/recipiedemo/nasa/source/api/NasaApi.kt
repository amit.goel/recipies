package com.ghn.recipiedemo.nasa.source.api

import com.ghn.recipiedemo.BuildConfig
import com.ghn.recipiedemo.nasa.data.models.response.NasaImage
import com.ghn.recipiedemo.nasa.data.models.response.PictureOfTheDay
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

private const val NASA_IMAGE_URL = "https://images-api.nasa.gov/search?media_type=image&page=1"

interface NasaApi {

    @GET("planetary/apod")
    suspend fun pictureOfTheDay(
        @Query("date") date: String,
        @Query("api_key") key: String = BuildConfig.NASA_API_KEY
    ): Response<PictureOfTheDay>

    @GET
    suspend fun getImages(
        @Url url: String = NASA_IMAGE_URL
    ): Response<NasaImage>
}

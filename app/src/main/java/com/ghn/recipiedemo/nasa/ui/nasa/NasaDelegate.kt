package com.ghn.recipiedemo.nasa.ui.nasa

import android.view.ViewGroup
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestOptions
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.nasa.data.models.response.PictureOfTheDay
import com.ghn.recipiedemo.shared.adapter.ViewType
import com.ghn.recipiedemo.shared.adapter.ViewTypeDelegateAdapter
import com.ghn.recipiedemo.shared.extensions.getImageFromURL
import com.ghn.recipiedemo.shared.extensions.inflate
import com.ghn.recipiedemo.shared.util.RoundedCornersTransformation
import kotlinx.android.synthetic.main.item_nasa.view.*

class NasaDelegate(
    private val itemListener: (palette: Palette) -> Unit
) : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup) = NasaViewHolder(parent)

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: ViewType,
        payloads: MutableList<Any>
    ) {
        (holder as NasaViewHolder).bind(item as PictureOfTheDay)
    }

    inner class NasaViewHolder(val parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_nasa)) {

        fun bind(item: PictureOfTheDay) {
            with(itemView) {
                text_picture_of_the_day.text = item.title
                picture_date.text = item.date

                val roundedType = RoundedCornersTransformation.CornerType.ALL
                picture_of_the_day.clipToOutline = true

                picture_of_the_day.getImageFromURL(
                    item.url!!,
                    RequestOptions().placeholder(R.drawable.image_background)
//                        .error(drawable)
                        .transforms(
                            CenterCrop(),
                            RoundedCornersTransformation(35, 0, roundedType)
                        )
                )
            }
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {}
    override fun onViewDetached(holder: RecyclerView.ViewHolder) {}
}

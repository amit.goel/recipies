package com.ghn.recipiedemo.nasa.ui.nasa

import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.palette.graphics.Palette
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.databinding.FragmentNasaBinding
import com.ghn.recipiedemo.nasa.data.models.response.PictureOfTheDay
import com.ghn.recipiedemo.shared.extensions.loadImage
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber

class NasaFragment : Fragment() {

    private lateinit var binding: FragmentNasaBinding

    private val viewModel: NasaViewModel by sharedViewModel()
    private val actionBar by lazy {
        (activity as? AppCompatActivity)?.supportActionBar
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.getPicture()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = FragmentNasaBinding.inflate(layoutInflater)
        return binding.root
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.picture.observe(this) { result ->
            when (result) {
                is Result.Loading -> Timber.d("Loading")
                is Result.Success -> renderView(result.data)
                is Result.Error -> handleError(result.throwable)
            }
        }

        binding.pictureOfTheDayTitle.setOnClickListener {
            view.findNavController().navigate(R.id.action_nav_nasa_home_to_nav_nasa_picture_gallery)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        actionBar?.hide()
    }

    private fun renderView(data: PictureOfTheDay) {
        binding.pictureOfTheDay.loadImage(
            data.hdUrl!!,
            object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {

                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    if (resource != null) {
                        val palette = Palette.from(resource.toBitmap()).generate()
                        val colorDark = palette.swatches[0]?.rgb
                            ?: ContextCompat.getColor(requireContext(), R.color.colorPrimary)

                        binding.pictureOfTheDayTitle.strokeColor = ColorStateList.valueOf(colorDark)
                        activity?.window?.apply {
                            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                            statusBarColor = colorDark
                        }
                        return false
                    }
                    return false
                }
            }
        )
    }

    private fun handleError(throwable: Throwable) {
        Timber.d(throwable)
    }
}

package com.ghn.recipiedemo.nasa.ui.nasa.images

import android.util.SparseArray
import com.ghn.recipiedemo.shared.adapter.AdapterConstants
import com.ghn.recipiedemo.shared.adapter.BaseAdapter
import com.ghn.recipiedemo.shared.adapter.ViewType
import com.ghn.recipiedemo.shared.adapter.ViewTypeDelegateAdapter

class NasaImageAdapter(
    private val itemListener: () -> Unit
) : BaseAdapter() {

    override fun setupDelegateAdapters(delegateAdapters: SparseArray<ViewTypeDelegateAdapter>) {
        delegateAdapters.apply {
            put(AdapterConstants.NASA_IMAGE, NasaImageDelegate(itemListener))
        }
    }

    override fun addItems(itemViews: MutableList<ViewType>) {
        val count = itemCount
        items.addAll(itemViews)
        notifyItemRangeInserted(count + 1, itemViews.size)
    }
}

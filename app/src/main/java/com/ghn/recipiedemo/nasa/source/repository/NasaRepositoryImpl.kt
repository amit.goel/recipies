package com.ghn.recipiedemo.nasa.source.repository

import com.ghn.recipiedemo.nasa.data.models.response.NasaImage
import com.ghn.recipiedemo.nasa.data.models.response.PictureOfTheDay
import com.ghn.recipiedemo.nasa.source.base.NasaRemoteDataSource
import com.ghn.recipiedemo.nasa.source.base.NasaRepository
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.flow.Flow

class NasaRepositoryImpl(
    private val remoteDataSource: NasaRemoteDataSource
) : NasaRepository {
    override suspend fun getPictureOfTheDay(date: String): Result<PictureOfTheDay> {
        return remoteDataSource.getPictureOfTheDay(date)
    }

    override suspend fun getNasaImages(query: String): Result<NasaImage> {
        return remoteDataSource.getNasaImages(query)
    }

    override fun getPictureOfTheDayFlow(
        date: String
    ): Flow<Result<PictureOfTheDay>> {
        return remoteDataSource.getPictureOfTheDayFlow(date)
    }
}

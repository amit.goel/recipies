package com.ghn.recipiedemo.nasa.ui.nasa.images

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.nasa.data.models.response.NasaImage
import com.ghn.recipiedemo.shared.adapter.ViewType
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.android.synthetic.main.fragment_nasa_images.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class NasaImageFragment : Fragment() {

    private var adapter: NasaImageAdapter? = null

    private val viewModel: NasaImageViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_nasa_images, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (adapter == null) {
            adapter = NasaImageAdapter(nasaImageListener())
        }

        if (nasa_images_rv.adapter == null) {
            configureAdapter()
        }

        viewModel.getImages()

        viewModel.nasaImages.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Success -> renderView(result.data)
                is Result.Error -> handleError(result.throwable)
            }
        }
    }

    private fun configureAdapter() {
        nasa_images_rv.apply {
            itemAnimator = null
            adapter = this@NasaImageFragment.adapter
        }
    }

    private fun renderView(data: NasaImage) {
        adapter?.addItems(data.collection?.items?.toMutableList<ViewType>() ?: mutableListOf())
    }

    private fun handleError(throwable: Throwable) {
        Timber.d(throwable)
    }

    private fun nasaImageListener(): () -> Unit {
        return {
        }
    }
}

package com.ghn.recipiedemo.nasa.data.models.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Link(
    @SerialName("prompt") val prompt: String? = null,
    @SerialName("rel") val rel: String? = null,
    @SerialName("href") val href: String? = null
) : Parcelable

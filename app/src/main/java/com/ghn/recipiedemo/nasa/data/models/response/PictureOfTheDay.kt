package com.ghn.recipiedemo.nasa.data.models.response

import android.os.Parcelable
import com.ghn.recipiedemo.shared.adapter.AdapterConstants
import com.ghn.recipiedemo.shared.adapter.ViewType
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class PictureOfTheDay(
    @SerialName("copyright") val copyright: String? = null,
    @SerialName("date") val date: String? = null,
    @SerialName("explanation") val explanation: String? = null,
    @SerialName("hdurl") val hdUrl: String? = null,
    @SerialName("media_type") val mediaType: String? = null,
    @SerialName("service_version") val serviceVersion: String? = null,
    @SerialName("title") val title: String? = null,
    @SerialName("url") val url: String? = null
) : Parcelable, ViewType {

    override fun getViewType(): Int = AdapterConstants.NASA
}

package com.ghn.recipiedemo.nasa.data.models.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Metadata(
    @SerialName("total_hits") val totalHits: Int? = null
) : Parcelable

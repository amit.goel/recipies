package com.ghn.recipiedemo.nasa.data.models.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class Data(
    @SerialName("nasa_id") val nasaId: String? = null,
    @SerialName("title") val title: String? = null,
    @SerialName("center") val center: String? = null,
    @SerialName("keywords") val keywords: List<String>? = null,
    @SerialName("date_created") val dateCreated: String? = null,
    @SerialName("media_type") val mediaType: String? = null,
    @SerialName("description") val description: String? = null
) : Parcelable

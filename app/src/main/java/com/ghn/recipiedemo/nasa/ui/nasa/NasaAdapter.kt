package com.ghn.recipiedemo.nasa.ui.nasa

import android.util.SparseArray
import androidx.palette.graphics.Palette
import com.ghn.recipiedemo.shared.adapter.AdapterConstants
import com.ghn.recipiedemo.shared.adapter.BaseAdapter
import com.ghn.recipiedemo.shared.adapter.ViewType
import com.ghn.recipiedemo.shared.adapter.ViewTypeDelegateAdapter

class NasaAdapter(
    private val itemListener: (Palette) -> Unit
) : BaseAdapter() {

    override fun setupDelegateAdapters(delegateAdapters: SparseArray<ViewTypeDelegateAdapter>) {
        delegateAdapters.apply {
            put(AdapterConstants.NASA, NasaDelegate(itemListener))
        }
    }

    override fun addItems(itemViews: MutableList<ViewType>) {
        val count = itemCount
        items.addAll(itemViews)
        notifyItemRangeInserted(count + 1, itemViews.size)
    }
}

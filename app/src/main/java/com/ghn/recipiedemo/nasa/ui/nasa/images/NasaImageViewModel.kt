package com.ghn.recipiedemo.nasa.ui.nasa.images

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ghn.recipiedemo.nasa.data.models.response.NasaImage
import com.ghn.recipiedemo.nasa.source.base.NasaRepository
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NasaImageViewModel(
    private val nasaRepository: NasaRepository
) : ViewModel() {

    private val _nasaImages = MutableLiveData<Result<NasaImage>>()

    val nasaImages: LiveData<Result<NasaImage>> = _nasaImages

    fun getImages(query: String = "") {
        viewModelScope.launch(Dispatchers.IO) {
            val result = nasaRepository.getNasaImages(query)
            _nasaImages.postValue(result)
        }
    }
}

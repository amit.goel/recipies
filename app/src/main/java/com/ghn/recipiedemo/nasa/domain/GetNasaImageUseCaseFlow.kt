package com.ghn.recipiedemo.nasa.domain

import com.ghn.recipiedemo.nasa.data.models.response.PictureOfTheDay
import com.ghn.recipiedemo.nasa.source.base.NasaRepository
import com.ghn.recipiedemo.shared.domain.FlowUseCase
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow

open class GetNasaImageUseCaseFlow(
    private val nasaRepository: NasaRepository,
    defaultDispatcher: CoroutineDispatcher
) : FlowUseCase<String, PictureOfTheDay>(defaultDispatcher) {

    override fun execute(parameters: String): Flow<Result<PictureOfTheDay>> =
        nasaRepository.getPictureOfTheDayFlow(parameters)
}

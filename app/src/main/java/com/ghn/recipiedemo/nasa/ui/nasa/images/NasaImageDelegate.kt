package com.ghn.recipiedemo.nasa.ui.nasa.images

import android.graphics.Bitmap
import android.view.ViewGroup
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.ghn.recipiedemo.GlideApp
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.nasa.data.models.response.Item
import com.ghn.recipiedemo.shared.adapter.ViewType
import com.ghn.recipiedemo.shared.adapter.ViewTypeDelegateAdapter
import com.ghn.recipiedemo.shared.extensions.inflate
import kotlinx.android.synthetic.main.item_nasa_image.view.*

class NasaImageDelegate(
    private val itemListener: () -> Unit
) : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup) = NasaImageViewHolder(parent)

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        item: ViewType,
        payloads: MutableList<Any>
    ) {
        (holder as NasaImageViewHolder).bind(item as Item)
    }

    inner class NasaImageViewHolder(val parent: ViewGroup) :
        RecyclerView.ViewHolder(parent.inflate(R.layout.item_nasa_image)) {

        fun bind(item: Item) {
            with(itemView) {
                item.links?.get(0)?.href?.let {
                    GlideApp.with(context!!)
                        .asBitmap()
                        .load(it)
                        .listener(object : RequestListener<Bitmap?> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any?,
                                target: com.bumptech.glide.request.target.Target<Bitmap?>?,
                                isFirstResource: Boolean
                            ): Boolean {

                                return false
                            }

                            override fun onResourceReady(
                                resource: Bitmap?,
                                model: Any?,
                                target: com.bumptech.glide.request.target.Target<Bitmap?>?,
                                dataSource: DataSource?,
                                isFirstResource: Boolean
                            ): Boolean {
                                if (resource != null) {
                                    val palette = Palette.from(resource).generate()
                                    nasa_image.setBackgroundColor(palette.dominantSwatch?.rgb!!)
//                                val textColor = palette.darkVibrantSwatch?.titleTextColor
//                                text_picture_of_the_day.setBackgroundColor(palette.darkVibrantSwatch?.rgb!!)
//                                text_picture_of_the_day.setTextColor(textColor!!)
//
//                                text_picture_description.setBackgroundColor(palette.dominantSwatch?.rgb!!)
//                                text_picture_description.setTextColor(palette.dominantSwatch?.bodyTextColor!!)
//
//                                picture_date.setTextColor(palette.dominantSwatch?.bodyTextColor!!)
                                }
                                return false
                            }
                        })
                        .into(nasa_image)
                }

                nasa_image_keywords.text = item.itemData?.get(0)?.keywords?.joinToString()
                nasa_image_title.text = item.itemData?.get(0)?.title
            }
        }
    }

    override fun onViewDetached(holder: RecyclerView.ViewHolder) {}
    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {}
}

package com.ghn.recipiedemo.nasa.ui.nasa

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.ghn.recipiedemo.R
import com.ghn.recipiedemo.databinding.FragmentNasaImageOfTheDayBinding
import com.ghn.recipiedemo.nasa.data.models.response.PictureOfTheDay
import com.ghn.recipiedemo.shared.adapter.CenterZoomLayoutManager
import com.ghn.recipiedemo.shared.extensions.manipulateColor
import com.ghn.recipiedemo.shared.util.InfiniteScrollListener
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber

@ExperimentalCoroutinesApi
class NasaImageOfTheDayFragment : Fragment() {

    private lateinit var binding: FragmentNasaImageOfTheDayBinding

    private var adapter: NasaAdapter? = null
    private var endlessScrollListener: InfiniteScrollListener? = null

    private val viewModel: NasaViewModel by sharedViewModel()
    private val actionBar by lazy {
        (activity as? AppCompatActivity)?.supportActionBar
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = FragmentNasaImageOfTheDayBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.nasa_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_view) {
            Timber.d("Rock N Roll")
//            view?.let {
//                findNavController().navigate(R.id.action_nav_picture_of_the_day_to_nav_nasa_images)
//            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (adapter == null) {
            adapter = NasaAdapter(nasaListener())
        }

        if (binding.nasaRv.adapter == null) {
            configureAdapter()
        }

        viewModel.getPictureFlow()

        viewModel.pictures.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> Timber.d("Loading")
                is Result.Success -> renderView(result.data)
                is Result.Error -> handleError(result.throwable)
            }
        }
    }

    private fun configureAdapter() {
        binding.nasaRv.apply {
            val linearLayoutManager = CenterZoomLayoutManager(requireActivity())
            linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
            layoutManager = linearLayoutManager

            adapter = this@NasaImageOfTheDayFragment.adapter

            if (onFlingListener == null)
                PagerSnapHelper().attachToRecyclerView(this)

            endlessScrollListener = object :
                InfiniteScrollListener(layoutManager as LinearLayoutManager) {
                override fun onLoadMore(page: Int, count: Int, view: RecyclerView) {
                    viewModel.getPictureFlow()
                    Timber.d("Logging onLoadMore")
                }
            }

            endlessScrollListener?.let { addOnScrollListener(it) }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        val colorPrimary = ContextCompat.getColor(requireContext(), R.color.colorPrimary)
        val colorPrimaryDark = ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)

        actionBar?.setBackgroundDrawable(ColorDrawable(colorPrimary))
        activity?.window?.statusBarColor = colorPrimaryDark
    }

    private fun renderView(data: PictureOfTheDay) {
        adapter?.addItems(mutableListOf(data))
    }

    private fun handleError(throwable: Throwable) {
        Timber.d(throwable)
    }

    private fun nasaListener(): (Palette) -> Unit {
        return { palette: Palette ->
            requireActivity().startPostponedEnterTransition()
            actionBar?.setDisplayHomeAsUpEnabled(true)

            val colorDark = palette.darkVibrantSwatch?.rgb?.manipulateColor(0.75f)
                ?: ContextCompat.getColor(requireContext(), R.color.colorPrimary)

            actionBar?.setBackgroundDrawable(ColorDrawable(colorDark))
            // set color status bar
            activity?.window?.statusBarColor =
                palette.darkVibrantSwatch?.rgb?.manipulateColor(
                    0.32f
                ) ?: ContextCompat.getColor(
                    requireContext(),
                    R.color.colorPrimaryDark
                )
        }
    }
}

package com.ghn.recipiedemo.nasa.domain

import com.ghn.recipiedemo.nasa.data.models.response.PictureOfTheDay
import com.ghn.recipiedemo.nasa.source.base.NasaRepository
import com.ghn.recipiedemo.shared.domain.SuspendUseCase
import com.ghn.recipiedemo.shared.util.Result
import kotlinx.coroutines.CoroutineDispatcher

class GetNasaImageUseCase(
    private val nasaRepository: NasaRepository,
    defaultDispatcher: CoroutineDispatcher
) : SuspendUseCase<String, PictureOfTheDay>(defaultDispatcher) {

    override suspend fun execute(parameters: String): PictureOfTheDay {
        return when (val result = nasaRepository.getPictureOfTheDay(parameters)) {
            is Result.Success -> {
                val updateResult = result.data
                updateResult
            }
            is Result.Error -> throw result.throwable
            else -> throw IllegalStateException()
        }
    }
}

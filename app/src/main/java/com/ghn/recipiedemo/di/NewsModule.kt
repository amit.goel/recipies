package com.ghn.recipiedemo.di

import com.ghn.recipiedemo.news.domain.GetNewsUseCaseFlow
import com.ghn.recipiedemo.news.source.api.NewsApi
import com.ghn.recipiedemo.news.source.base.NewsRemoteDataSource
import com.ghn.recipiedemo.news.source.base.NewsRepository
import com.ghn.recipiedemo.news.source.remote.NewsRemoteDataSourceImpl
import com.ghn.recipiedemo.news.source.repository.NewsRepositoryImpl
import com.ghn.recipiedemo.news.ui.NewsViewModel
import com.ghn.recipiedemo.shared.extensions.createRetrofit
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

@ExperimentalCoroutinesApi
val newsModule = module {

    single { provideNewsAPI(retrofit = get(named("newsRetrofit"))) }

    single(named<NewsRemoteDataSource>()) { NewsRemoteDataSourceImpl(get()) }

    single(named<NewsRepository>()) { NewsRepositoryImpl(get(named<NewsRemoteDataSource>())) }

    single { GetNewsUseCaseFlow(get(named<NewsRepository>()), get(named("dispatchersIO"))) }

    viewModel { NewsViewModel(get()) }
}

private fun provideNewsAPI(retrofit: Retrofit): NewsApi = retrofit.createRetrofit()

package com.ghn.recipiedemo.di

import com.ghn.recipiedemo.shared.extensions.createRetrofit
import com.ghn.recipiedemo.weather.WeatherViewModel
import com.ghn.recipiedemo.weather.data.LocationDataSourceImpl
import com.ghn.recipiedemo.weather.data.api.WeatherApi
import com.ghn.recipiedemo.weather.data.base.LocationDataSource
import com.ghn.recipiedemo.weather.data.base.WeatherRemoteDataSource
import com.ghn.recipiedemo.weather.data.remote.WeatherRemoteDataSourceImpl
import com.ghn.recipiedemo.weather.data.repository.LocationRepositoryImpl
import com.ghn.recipiedemo.weather.data.repository.WeatherRepositoryImpl
import com.ghn.recipiedemo.weather.domain.FetchWeatherUseCase
import com.ghn.recipiedemo.weather.domain.GetLocationUseCase
import com.ghn.recipiedemo.weather.domain.repository.LocationRepository
import com.ghn.recipiedemo.weather.domain.repository.WeatherRepository
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val weatherModule = module {

    single { provideWeatherAPI(retrofit = get(named("weatherRetrofit"))) }

    single(named<WeatherRemoteDataSource>()) { WeatherRemoteDataSourceImpl(get()) }

    single(named<WeatherRepository>()) {
        WeatherRepositoryImpl(
            get(named<WeatherRemoteDataSource>())
        )
    }

    factory {
        FetchWeatherUseCase(
            get(named<WeatherRepository>()),
            get(named("dispatchersIO"))
        )
    }

    single(named<LocationDataSource>()) { LocationDataSourceImpl(get(), get()) }

    single(named<LocationRepository>()) {
        LocationRepositoryImpl(
            get(named<LocationDataSource>())
        )
    }

    factory {
        GetLocationUseCase(
            get(named<LocationRepository>()),
            get(named("dispatchersIO"))
        )
    }

    viewModel { WeatherViewModel(get(), get()) }
}

private fun provideWeatherAPI(retrofit: Retrofit): WeatherApi = retrofit.createRetrofit()

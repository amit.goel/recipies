package com.ghn.recipiedemo.di

import android.content.Context
import com.ghn.recipiedemo.data.AppDatabase
import org.koin.dsl.module

val localCacheModule = module {
    single { provideRoom(get()) }
}

private fun provideRoom(
    context: Context
): AppDatabase {
    return AppDatabase.getInstance(context)
}

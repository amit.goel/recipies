package com.ghn.recipiedemo.di

import android.content.Context
import android.location.Geocoder
import android.location.LocationManager
import org.koin.dsl.module
import java.util.*

val locationModule = module {

    single { provideLocationManager(get()) }
    single { provideGeocoder(get()) }
}

private fun provideLocationManager(context: Context): LocationManager =
    context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

private fun provideGeocoder(context: Context): Geocoder =
    Geocoder(context, Locale.getDefault())

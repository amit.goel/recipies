package com.ghn.recipiedemo.di

import com.ghn.recipiedemo.gallery.data.source.api.RecipeAPI
import com.ghn.recipiedemo.gallery.data.source.base.GalleryRemoteDataSource
import com.ghn.recipiedemo.gallery.data.source.base.GalleryRepository
import com.ghn.recipiedemo.gallery.data.source.remote.GalleryRemoteDataSourceImpl
import com.ghn.recipiedemo.gallery.data.source.repository.GalleryRepositoryImpl
import com.ghn.recipiedemo.gallery.ui.GalleryViewModel
import com.ghn.recipiedemo.shared.extensions.createRetrofit
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val recipeModule = module {

    single { provideMealAPI(retrofit = get(named("recipeRetrofit"))) }

    single(named<GalleryRemoteDataSource>()) {
        GalleryRemoteDataSourceImpl(api = get())
    }

    single(named<GalleryRepository>()) {
        GalleryRepositoryImpl(get(named<GalleryRemoteDataSource>()))
    }

    viewModel { GalleryViewModel(get(named<GalleryRepository>())) }
}

private fun provideMealAPI(retrofit: Retrofit): RecipeAPI = retrofit.createRetrofit()

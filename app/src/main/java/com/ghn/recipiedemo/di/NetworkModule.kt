@file:Suppress("SpellCheckingInspection")

package com.ghn.recipiedemo.di

import com.facebook.flipper.plugins.network.FlipperOkhttpInterceptor
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import com.ghn.recipiedemo.BuildConfig
import com.ghn.recipiedemo.shared.util.SingleToArrayAdapter
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

@UnstableDefault
val networkModule = module {

    single { provideHTTPLoggingInterceptor() }
    single(named("kotlinXConverter")) { provideKotlinConverterFactory() }
    single(named("moshiConverter")) { provideMoshiConverterFactory() }
    single { provideNetworkFlipperPlugin() }
    single { provideDefaultOkHttpClient(get(), get()) }

    single(named("dispatchersIO")) { provideDispatchersIO() }

    single(named("recipeRetrofit")) {
        provideRecipeRetrofit(
            get(),
            get(named("kotlinXConverter"))
        )
    }

    single(named("nasaRetrofit")) {
        provideNasaRetrofit(
            get(),
            get(named("kotlinXConverter"))
        )
    }

    single(named("newsRetrofit")) {
        provideNewsRetrofit(
            get(),
            get(named("kotlinXConverter"))
        )
    }

    single(named("weatherRetrofit")) {
        provideWeatherRetrofit(
            get(),
            get(named("moshiConverter"))
        )
    }

    single(named("stocksRetrofit")) {
        provideStocksRetrofit(
            get(),
            get(named("moshiConverter"))
        )
    }

    single(named("symbolsRetrofit")) {
        provideSymbolsRetrofit(
            get(),
            get(named("moshiConverter"))
        )
    }

    single(named("tradierRetrofit")) {
        provideTradierRetrofit(
            get(),
            get(named("moshiConverter"))
        )
    }
}

private fun provideNetworkFlipperPlugin() = NetworkFlipperPlugin()

private fun provideDispatchersIO() = Dispatchers.IO

private fun provideHTTPLoggingInterceptor(): HttpLoggingInterceptor =
    HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

@UnstableDefault
private fun provideKotlinConverterFactory(): Converter.Factory =
    Json(JsonConfiguration(strictMode = false)).asConverterFactory("application/json".toMediaType())

private fun provideMoshiConverterFactory(): Converter.Factory =
    MoshiConverterFactory.create(
        Moshi.Builder()
            .add(SingleToArrayAdapter.INSTANCE)
            .build()
    )

private fun provideDefaultOkHttpClient(
    httpLoggingInterceptor: HttpLoggingInterceptor,
    networkFlipperPlugin: NetworkFlipperPlugin
): OkHttpClient {

    val okHttpClientBuilder = OkHttpClient.Builder()
        .addInterceptor(FlipperOkhttpInterceptor(networkFlipperPlugin))
        .connectTimeout(30.toLong(), TimeUnit.SECONDS)
        .readTimeout(60.toLong(), TimeUnit.SECONDS)
        .writeTimeout(60.toLong(), TimeUnit.SECONDS)
        .callTimeout(180.toLong(), TimeUnit.SECONDS)

    if (BuildConfig.DEBUG) {
        okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)
    }

    return okHttpClientBuilder.build()
}

private fun provideRecipeRetrofit(
    okHttpClient: OkHttpClient,
    factory: Converter.Factory
): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.HOST)
        .addConverterFactory(factory)
        .client(okHttpClient)
        .build()
}

private fun provideNasaRetrofit(okHttpClient: OkHttpClient, factory: Converter.Factory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.NASA_HOST)
        .addConverterFactory(factory)
        .client(okHttpClient)
        .build()
}

fun provideNewsRetrofit(okHttpClient: OkHttpClient, factory: Converter.Factory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.NEWS_HOST)
        .addConverterFactory(factory)
        .client(okHttpClient)
        .build()
}

fun provideWeatherRetrofit(okHttpClient: OkHttpClient, factory: Converter.Factory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.WEATHER_HOST)
        .addConverterFactory(factory)
        .client(okHttpClient)
        .build()
}

fun provideStocksRetrofit(okHttpClient: OkHttpClient, factory: Converter.Factory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.STOCKS_HOST)
        .addConverterFactory(factory)
        .client(okHttpClient)
        .build()
}

fun provideSymbolsRetrofit(okHttpClient: OkHttpClient, factory: Converter.Factory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.SYMBOLS_HOST)
        .addConverterFactory(factory)
        .client(okHttpClient)
        .build()
}

fun provideTradierRetrofit(okHttpClient: OkHttpClient, factory: Converter.Factory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.TRADIER_BASE_URL)
        .addConverterFactory(factory)
        .client(okHttpClient)
        .build()
}

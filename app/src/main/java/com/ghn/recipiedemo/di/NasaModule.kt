package com.ghn.recipiedemo.di

import com.ghn.recipiedemo.nasa.domain.GetNasaImageUseCase
import com.ghn.recipiedemo.nasa.domain.GetNasaImageUseCaseFlow
import com.ghn.recipiedemo.nasa.source.api.NasaApi
import com.ghn.recipiedemo.nasa.source.base.NasaRemoteDataSource
import com.ghn.recipiedemo.nasa.source.base.NasaRepository
import com.ghn.recipiedemo.nasa.source.remote.NasaRemoteDataSourceImpl
import com.ghn.recipiedemo.nasa.source.repository.NasaRepositoryImpl
import com.ghn.recipiedemo.nasa.ui.nasa.NasaViewModel
import com.ghn.recipiedemo.nasa.ui.nasa.images.NasaImageViewModel
import com.ghn.recipiedemo.shared.extensions.createRetrofit
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

@ExperimentalCoroutinesApi
val nasaModule = module {

    single { provideNasaAPI(retrofit = get(named("nasaRetrofit"))) }

    single(named<NasaRemoteDataSource>()) { NasaRemoteDataSourceImpl(get()) }

    single(named<NasaRepository>()) { NasaRepositoryImpl(get(named<NasaRemoteDataSource>())) }

    single { GetNasaImageUseCase(get(named<NasaRepository>()), get(named("dispatchersIO"))) }
    single { GetNasaImageUseCaseFlow(get(named<NasaRepository>()), get(named("dispatchersIO"))) }

    viewModel { NasaViewModel(get(), get()) }

    viewModel { NasaImageViewModel(get(named<NasaRepository>())) }
}

private fun provideNasaAPI(retrofit: Retrofit): NasaApi = retrofit.createRetrofit()

package com.ghn.recipiedemo.di

import com.ghn.recipiedemo.portfolio.data.api.SymbolsApi
import com.ghn.recipiedemo.portfolio.data.api.TradierSearchApi
import com.ghn.recipiedemo.portfolio.data.base.StocksLocalDataSource
import com.ghn.recipiedemo.portfolio.data.base.StocksRemoteDataSource
import com.ghn.recipiedemo.portfolio.data.local.StocksLocalDataSourceImpl
import com.ghn.recipiedemo.portfolio.data.remote.StocksRemoteDataSourceImpl
import com.ghn.recipiedemo.portfolio.data.repository.StocksRepositoryImpl
import com.ghn.recipiedemo.portfolio.domain.AddToWatchlistUseCase
import com.ghn.recipiedemo.portfolio.domain.FetchQuotesUseCase
import com.ghn.recipiedemo.portfolio.domain.GetQuotesUseCaseFlow
import com.ghn.recipiedemo.portfolio.domain.SearchSymbolsUseCase
import com.ghn.recipiedemo.portfolio.domain.repository.StocksRepository
import com.ghn.recipiedemo.portfolio.presentation.portfolio.PortfolioViewModel
import com.ghn.recipiedemo.portfolio.presentation.search.StockSearchViewModel
import com.ghn.recipiedemo.shared.extensions.createRetrofit
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val stocksModule = module {

    single(named<SymbolsApi>()) { provideSymbolsAPI(retrofit = get(named("symbolsRetrofit"))) }
    single(named<TradierSearchApi>()) { provideTradierAPI(retrofit = get(named("tradierRetrofit"))) }

    single(named<StocksRemoteDataSource>()) {
        StocksRemoteDataSourceImpl(
            get(named<SymbolsApi>()),
            get(named<TradierSearchApi>())
        )
    }

    single(named<StocksLocalDataSource>()) { StocksLocalDataSourceImpl(get()) }

    single(named<StocksRepository>()) {
        StocksRepositoryImpl(
            get(named<StocksRemoteDataSource>()),
            get(named<StocksLocalDataSource>())
        )
    }

    factory {
        GetQuotesUseCaseFlow(get(named<StocksRepository>()), get(named("dispatchersIO")))
    }

    factory {
        FetchQuotesUseCase(get(named<StocksRepository>()), get(named("dispatchersIO")))
    }

    factory {
        SearchSymbolsUseCase(get(named<StocksRepository>()), get(named("dispatchersIO")))
    }

    factory {
        AddToWatchlistUseCase(get(named<StocksRepository>()), get(named("dispatchersIO")))
    }

    viewModel {
        PortfolioViewModel(
            get(),
            get()
        )
    }
    viewModel {
        StockSearchViewModel(get(), get())
    }
}

private fun provideSymbolsAPI(retrofit: Retrofit): SymbolsApi = retrofit.createRetrofit()

private fun provideTradierAPI(retrofit: Retrofit): TradierSearchApi = retrofit.createRetrofit()

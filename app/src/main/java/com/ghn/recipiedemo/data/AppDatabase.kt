package com.ghn.recipiedemo.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ghn.recipiedemo.data.dao.quotesDao
import com.ghn.recipiedemo.data.dao.recipeDao
import com.ghn.recipiedemo.data.models.RecipeModule
import com.ghn.recipiedemo.portfolio.data.models.QuoteModule

@Database(
    entities = [
        (RecipeModule::class),
        (QuoteModule::class)
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun recipeDao(): recipeDao
    abstract fun quotesDao(): quotesDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        private const val DB_NAME = "demoAppdb"

        fun getInstance(context: Context): AppDatabase = INSTANCE ?: synchronized(this) {
            INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
                .build()
        }

        fun destroyDataBase() {
            INSTANCE = null
        }
    }
}

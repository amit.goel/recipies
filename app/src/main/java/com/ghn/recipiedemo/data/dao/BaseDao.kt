package com.ghn.recipiedemo.data.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<T> {

    /**
     * Insert an object in the database.
     *
     * @param obj the object to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(obj: T): Long

    /**
     * Insert an array of objects in the database.
     *
     * @param items the objects to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    suspend fun insert(items: List<T>): List<Long>

    /**
     * Update an object from the database.
     *
     * @param obj the object to be updated
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(obj: T): Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(items: List<T>): Int

    /**
     * Delete an object from the database
     *
     * @param obj the object to be deleted
     */
    @Delete
    suspend fun delete(obj: T)

//    @Transaction
//    fun upsert(obj: T) {
//        val id = insert(obj)
//        if (id == -1L) {
//            update(obj)
//        }
//    }
//
//    @Transaction
//    fun upsert(items: List<T>) {
//        val insertResults = insert(items)
//        val updateList = arrayListOf<T>()
//
//        insertResults.forEachIndexed { index, l ->
//            if (l == -1L) {
//                updateList.add(items[index])
//            }
//
//            if (updateList.isNotEmpty()) {
//                update(updateList)
//            }
//        }
//    }
}

package com.ghn.recipiedemo.data.dao

import androidx.room.Dao
import androidx.room.Query
import com.ghn.recipiedemo.portfolio.data.models.QuoteModule
import kotlinx.coroutines.flow.Flow

@Dao
interface quotesDao : BaseDao<QuoteModule> {

    @Query("SELECT * FROM quote_module")
    fun getAllFlow(): Flow<List<QuoteModule>>

    @Query("SELECT * FROM quote_module")
    suspend fun getAll(): List<QuoteModule>

    @Query("SELECT * FROM quote_module where symbol = :symbol")
    suspend fun getSymbol(symbol: String): QuoteModule
}

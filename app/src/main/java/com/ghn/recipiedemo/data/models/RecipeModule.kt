package com.ghn.recipiedemo.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "recipe_module")
data class RecipeModule(
    @PrimaryKey(autoGenerate = false) var id: Long,
    @ColumnInfo(name = "name") val name: Int,
    @ColumnInfo(name = "thumbnail") val thumbnail: Int,
    @ColumnInfo(name = "description") val description: String
)

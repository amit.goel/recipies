package com.ghn.recipiedemo.data.dao

import androidx.room.Dao
import androidx.room.Query
import com.ghn.recipiedemo.data.models.RecipeModule

@Dao
interface recipeDao :
    BaseDao<RecipeModule> {

    @Query("SELECT * FROM recipe_module")
    suspend fun getAll(): List<RecipeModule>
}

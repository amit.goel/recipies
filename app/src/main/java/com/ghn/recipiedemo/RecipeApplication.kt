package com.ghn.recipiedemo

import android.app.Application
import com.facebook.flipper.android.AndroidFlipperClient
import com.facebook.flipper.android.utils.FlipperUtils
import com.facebook.flipper.plugins.databases.DatabasesFlipperPlugin
import com.facebook.flipper.plugins.inspector.DescriptorMapping
import com.facebook.flipper.plugins.inspector.InspectorFlipperPlugin
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import com.facebook.soloader.SoLoader
import com.ghn.recipiedemo.data.AppDatabase
import com.ghn.recipiedemo.di.localCacheModule
import com.ghn.recipiedemo.di.locationModule
import com.ghn.recipiedemo.di.nasaModule
import com.ghn.recipiedemo.di.networkModule
import com.ghn.recipiedemo.di.newsModule
import com.ghn.recipiedemo.di.recipeModule
import com.ghn.recipiedemo.di.stocksModule
import com.ghn.recipiedemo.di.weatherModule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.serialization.UnstableDefault
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class RecipeApplication : Application() {

    @UnstableDefault
    @ExperimentalCoroutinesApi
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())

        instance = this
        val koinModules = listOf(
            locationModule,
            localCacheModule,
            networkModule,
            recipeModule,
            nasaModule,
            newsModule,
            stocksModule,
            weatherModule
        )

        startKoin {
            androidLogger()
            androidContext(this@RecipeApplication)
            modules(koinModules)
        }

        AppDatabase.getInstance(this).openHelper.writableDatabase

        SoLoader.init(this, false)

        if (BuildConfig.DEBUG && FlipperUtils.shouldEnableFlipper(this)) {
            val networkFlipperPlugin by inject<NetworkFlipperPlugin>()

            val client = AndroidFlipperClient.getInstance(this)
            client.addPlugin(InspectorFlipperPlugin(this, DescriptorMapping.withDefaults()))
            client.addPlugin(DatabasesFlipperPlugin(this))
            client.addPlugin(networkFlipperPlugin)
            client.start()
        }
    }

    companion object {
        lateinit var instance: RecipeApplication
    }
}

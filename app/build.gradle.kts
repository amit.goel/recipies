import org.jmailen.gradle.kotlinter.tasks.LintTask

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
    id("kotlinx-serialization")
    id("androidx.navigation.safeargs")
    id("org.jmailen.kotlinter") version "2.2.0"
}

fun Project.propertyOrEmpty(name: String): String {
    val property = findProperty(name) as String?
    return property ?: ""
}

fun Project.buildConfigProperty(name: String) = "\"${propertyOrEmpty(name)}\""

android {
    buildToolsVersion(Apps.buildTools)
    compileSdkVersion(Apps.compileSdk)
    defaultConfig {
        applicationId = Apps.applicationId
        minSdkVersion(Apps.minSdk)
        targetSdkVersion(Apps.targetSdk)
        versionCode = Apps.versionCode
        versionName = Apps.versionName
        multiDexEnabled = true

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        buildConfigField("String", "HOST", Hosts.baseUrl)
        buildConfigField("String", "NASA_HOST", Hosts.nasaHost)
        buildConfigField("String", "NEWS_HOST", Hosts.newsHost)
        buildConfigField("String", "WEATHER_HOST", Hosts.weatherHost)
        buildConfigField("String", "STOCKS_HOST", Hosts.stocksHost)
        buildConfigField("String", "SYMBOLS_HOST", Hosts.symbolsHost)
        buildConfigField("String", "TRADIER_BASE_URL", Hosts.tradierBaseUrl)
        buildConfigField("String", "NASA_API_KEY", "\"" + getLocalProperty("nasa.api.key") + "\"")
        buildConfigField("String", "NEWS_API_KEY", "\"" + getLocalProperty("news.api.key") + "\"")

        buildConfigField(
            "String",
            "WEATHER_API_KEY",
            buildConfigProperty("WeatherApp_Api_Key")
        )
        buildConfigField(
            "String",
            "STOCKS_API_KEY",
            buildConfigProperty("Stocks_Api_Key")
        )
        buildConfigField(
            "String",
            "SYMBOLS_API_KEY",
            buildConfigProperty("Symbols_Api_Key")
        )
         buildConfigField(
            "String",
            "TRADIER_API_KEY",
            buildConfigProperty("Tradier_Api_Key")
        )
        buildConfigField("String", "MAPS_API_KEY", Hosts.mapsApiKey)
    }

    buildTypes {

        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    lintOptions {
        isAbortOnError = false
        isCheckReleaseBuilds = false
        disable("ContentDescription")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    sourceSets {
        //        test {
//            resources.srcDirs += ["sampledata"]
//        }
//        androidTest {
//            resources.srcDirs += ["sampledata"]
//        }
    }

    testOptions {
        unitTests.isIncludeAndroidResources = true
    }

    androidExtensions {
        isExperimental = true
    }

    viewBinding {
        isEnabled = true
    }

    packagingOptions {
        exclude("META-INF/kotlinx-coroutines-core.kotlin_module")
    }
}

dependencies {
    implementation(project(":common"))
    //implementation(mapOf("name" to Libs.tripleOG, "ext" to "jar"))
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(files("libs/TripleOG-android-0.0.1.jar"))
    implementation(Libs.kotlin)

    implementation(Libs.annotation)
    implementation(Libs.appcompat)
    implementation(Libs.archLifecycle)
    implementation(Libs.browser)
    implementation(Libs.constraintLayout)
    implementation(Libs.coreKtx)
    implementation(Libs.fragment)
    implementation(Libs.lifeCycle)
    implementation(Libs.material)
    implementation(Libs.navigation)
    implementation(Libs.navigationUI)
    implementation(Libs.palette)
    implementation(Libs.recyclerView)
    implementation(Libs.swipeRefresh)

    // OkHttp + Retrofit
    implementation(Libs.retrofit)
    implementation(Libs.retrofitMoshi)
    implementation(Libs.retrofitKotlinSerializer)
    implementation(Libs.okHttpInterceptor)

    // Koin
    implementation(Libs.koin)
    implementation(Libs.koinScope)
    implementation(Libs.koinViewModel)

    // Room
    implementation(Libs.room)
    implementation(Libs.roomRuntime)
    kapt(Libs.roomKapt)

    // Lifecycle Extensions
    implementation(Libs.lifecycleExtensions)
    implementation(Libs.lifecycleLiveData)
    implementation(Libs.lifecycleViewModel)
    implementation(Libs.lifecycleRuntime)

    // Image Handling
    implementation(Libs.glide)
    implementation(Libs.kenBurnsView)
    kapt(Libs.glideCompiler)

    // Logging
    implementation(Libs.timber)

    debugImplementation(TestLibs.junit)
    debugImplementation(TestLibs.junitAndroid)
    debugImplementation(TestLibs.espresso)
    debugImplementation(TestLibs.fragmentTest)

    // Android runner and rules support
    debugImplementation(TestLibs.testCore)
    debugImplementation(TestLibs.testRunner)
    debugImplementation(TestLibs.testRules)

    // Koin testing tools
    testImplementation(TestLibs.koinTest)

    // Mockito
    testImplementation(TestLibs.mockito)
    androidTestImplementation(TestLibs.mockitoAndroid)

    // Hamcrest
    testImplementation(TestLibs.hamcrest)
    testImplementation(TestLibs.archTestingVersion)

    // coroutines test
    testImplementation(TestLibs.coroutinesTest)

    // Kluent
    testImplementation(TestLibs.kluent)
    testImplementation(TestLibs.kluentAndroid)

    // mock web server
    testImplementation(TestLibs.mockWebServer)

    debugImplementation(TestLibs.debugDb)

    // flipper
    debugImplementation(TestLibs.flipper)
    debugImplementation(TestLibs.flipperNetwork)
    debugImplementation(TestLibs.soloader)

    releaseImplementation(TestLibs.flipperNoOp)
}

kotlinter {
    disabledRules = arrayOf("import-ordering", "no-wildcard-imports", "filename")
}

val ktLint by tasks.creating(LintTask::class) {
    group = "verification"
    source(files("src"))
    reports = mapOf(
        "plain" to file("build/lint-report.txt"),
        "json" to file("build/lint-report.json")
    )
    ignoreFailures = false
}

tasks.getByName("build").finalizedBy(ktLint)

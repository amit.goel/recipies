package com.ghn.common

import java.text.SimpleDateFormat
import java.util.*

actual fun platformName(): String {
    return "Android"
}

actual class KMPDate actual constructor(formatString: String) {
    private val dateFormat = SimpleDateFormat(formatString)

    actual fun asString(): String {
        return dateFormat.format(Date())
    }
}

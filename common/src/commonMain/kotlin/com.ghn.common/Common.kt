package com.ghn.common

expect fun platformName(): String

fun createApplicationScreenMessage(): String {
    return "Kotlin Rocks on ${platformName()}"
}

expect class KMPDate(formatString: String) {
    fun asString(): String
}

private val date = KMPDate("MMM dd")

fun hello(): String = "Hello, Android and iOS worlds, today is ${date.asString()}!"


package com.ghn.common

actual fun platformName(): String {
    return "iOS"
}

actual class KMPDate actual constructor(formatString: String) { // 1
    private val dateFormatter = NSDateFormatter().apply {
        // 2
        this.dateFormat = formatString
    }

    actual fun asString(): String {
        return formatter.stringFromDate(NSDate()) // 3
    }
}
